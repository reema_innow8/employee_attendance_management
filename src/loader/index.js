import React from "react";
import cx from "classnames";
let dots = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];

const Loader = ({ top = false }) => {
  return (
    <div className={cx("sk-circle-wrpper", { top: top })}>
      <div className="sk-circle">
        {dots.map((data, index) => (
          <div key={index} className={`sk-circle${index + 1} sk-child`} />
        ))}
      </div>
    </div>
  );
};
export default Loader;
