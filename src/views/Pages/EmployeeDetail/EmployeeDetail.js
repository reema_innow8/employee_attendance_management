import React, { Component } from "react";
import DefaultLayout from "../../../containers/layout";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { NotificationManager } from "react-notifications";
import { apiCall } from "../../../common/axios";
// import CommonPagination from "../../../common/pagination";
import {
  Card,
  CardBody,
  CardHeader,
  FormGroup,
  Label,
  Button,
  Row,
  Col,
  Table,
} from "reactstrap";
import Loader from "../../../loader/index";
import {
  dateDifference,
  minHrDayConverter,
} from "../../../common/calculateTime";

const workingTime = (data) => {
  let time = "";
  if (data) {
    if (data.days > 0) {
      if (data.days === 1) time = time.concat(data.days + " day");
      else time = time.concat(data.days + " days");
    }
    if (data.hours > 0) {
      if (data.hours === 1) time = time.concat(" " + data.hours + " hour");
      else time = time.concat(" " + data.hours + " hours");
    }
    if (data.minutes > 0) {
      if (data.minutes === 1)
        time = time.concat(" " + data.minutes + " minute");
      else time = time.concat(" " + data.minutes + " minutes");
    }
  }
  return time;
};

class EmployeeDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      endDate: new Date(),
      dateArr: [],
      time_total: "",
      loading: false,
      activePage: 1,
    };
  }

  handleDate = (type, date) => {
    this.setState({
      [type]: new Date(moment(date).format("LL")),
    });
  };

  calculateInOut = (arr) => {
    let effHrs = [];
    for (let data of arr) {
      data.entry.sort((a, b) => new Date(a) - new Date(b));
      data.exit.sort((a, b) => new Date(a) - new Date(b));
      const diffArr = [];
      let days = 0,
        hours = 0,
        minutes = 0;
      for (let i = 0; i < data.entry.length; i++) {
        const startDate = data.entry[i];
        const endDate = data.exit[i] || "";
        const diff = (endDate && dateDifference(startDate, endDate)) || "";
        if (diff) {
          if (diff.days) days += diff.days;
          if (diff.hours) hours += diff.hours;
          if (diff.minutes) minutes += diff.minutes;
        }
        const time = (diff && workingTime(diff)) || "";
        diffArr.push(time);
      }
      effHrs.push({
        ...data,
        diffArr,
        effTime: minHrDayConverter({ days, hours, minutes }),
      });
    }
    return effHrs;
  };

  handleSubmit = (start, end) => {
    const { startDate, endDate } = this.state;
    // const startDateFormat = moment(startDate).format("DD/MM/YYYY");
    // const endDateFormat = moment(endDate).format("DD/MM/YYYY");
    this.setState({
      loading: true,
    });
    const toDate = new Date(moment(endDate).format("LL"));
    const employeeObj = {
      id:
        this.props &&
        this.props.history &&
        this.props.history.location &&
        this.props.history.location.state &&
        this.props.history.location.state._id,
      from: new Date(moment(startDate).format("LL")),
      to: new Date(toDate.setDate(toDate.getDate() + 1)),
    };

    apiCall("attendance/fetch", employeeObj)
      .then((res) => {
        const arr = this.calculateInOut(res.data.data);
        this.setState({
          dateArr: arr,
        });
        if (res.data.status === 200) {
          this.setState({
            loading: false,
          });
        } else {
          this.setState({
            loading: false,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };
  handleBack = (e) => {
    e.preventDefault();
    this.props.history.push("/employees");
  };
  // handlePageChange = (pageNumber) => {
  //   this.setState({ activePage: pageNumber });
  // };

  render() {
    const { startDate, endDate, dateArr, loading } = this.state;

    const formattedStart = moment(startDate);
    const minEndDate = moment(formattedStart).add(30, "days");

    if (startDate > endDate) {
      NotificationManager.warning("Start Date should be less than End Date");
    }
    return (
      <div>
        <DefaultLayout>
          <Card>
            <Row>
              <Col xs="12">
                <CardHeader>
                  <Button
                    onClick={this.handleBack}
                    className="backBtn"
                    disabled={loading}
                  >
                    <i className="fas fa-arrow-left"></i>Back
                  </Button>
                </CardHeader>
                <CardHeader className="employee-form">
                  <div className="employee-filled-form">
                    <Row className="d-flex">
                      <Col sm={6}>
                        <FormGroup>
                          <Label>
                            <b>Employee Id:</b>
                          </Label>
                          {this.props.history.location.state.empId}
                        </FormGroup>
                      </Col>
                      <Col sm={6}>
                        <FormGroup>
                          <Label>
                            <b>Name:</b>{" "}
                          </Label>
                          <span className="text-capitalize">
                            {this.props.history.location.state.name}
                          </span>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row className="d-flex">
                      <Col sm={6}>
                        <FormGroup>
                          <Label for="from" className="mb-0">
                            <b>From:</b>
                          </Label>
                          <DatePicker
                            selected={this.state.startDate}
                            onChange={(date) =>
                              this.handleDate("startDate", date)
                            }
                            dateFormat="dd/MM/yyyy"
                            maxDate={new Date()}
                            // minDate={minStartDate._d}
                          />
                        </FormGroup>
                      </Col>
                      <Col sm={6}>
                        <FormGroup>
                          <Label for="to" className="mb-0">
                            <b>To:</b>
                          </Label>
                          <DatePicker
                            selected={this.state.endDate}
                            onChange={(date) =>
                              this.handleDate("endDate", date)
                            }
                            dateFormat="dd/MM/yyyy"
                            // minDate={startDate}
                            // maxDate={new Date()}
                            maxDate={minEndDate._d}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row className="text-center mt-2">
                      <Col>
                        <Button
                          color="success"
                          disabled={loading || startDate > endDate}
                          onClick={(e) => this.handleSubmit(startDate, endDate)}
                        >
                          Submit<i className="fas fa-arrow-right ml-1"></i>
                        </Button>
                      </Col>
                    </Row>
                  </div>
                </CardHeader>
              </Col>
            </Row>
            <div
              className={`employee-detailTable  f-w-loader ${
                loading ? "loader" : ""
              }`}
            >
              {loading && <Loader top />}
              {dateArr && dateArr.length > 0
                ? dateArr.map((data, index) => {
                    data.entry.sort((a, b) => new Date(a) - new Date(b));
                    data.exit.sort((a, b) => new Date(a) - new Date(b));
                    return (
                      <div className="mb-4" key={index}>
                        {!loading && (
                          <CardBody>
                            <h2 className="main-date">
                              {moment(data._id).format("MMMM D, YYYY")}
                            </h2>
                          </CardBody>
                        )}
                        <div>
                          {/* <Table responsive striped>
                            <thead>
                              <tr>
                                <th className="text-nowrap">In</th>
                                <th>Out</th>
                                <th>Time</th>
                              </tr>
                            </thead>
                            <tbody>
                              {data.entry &&
                                data.entry.map((startDate, index) => {
                                 
                                  const endDate = data.exit[index] || "";
                                  time_in = moment(startDate).format("hh:mm A");
                                  time_out =
                                    (endDate &&
                                      moment(endDate).format("hh:mm A")) ||
                                    "";
                                  const diff =
                                    (endDate &&
                                      dateDifference(startDate, endDate)) ||
                                    "";
                                  return (
                                    <tr>
                                      <td>{time_in}</td>
                                      <td>{time_out || ""}</td>
                                      <td>
                                        {(diff && workingTime(diff)) || ""}
                                      </td>
                                    </tr>
                                  );
                                })}
                            </tbody>
                          </Table> */}
                          <Table responsive className="mb-0">
                            <tbody>
                              <tr className="text-nowrap">
                                <th>In</th>
                                {data.entry.map((item, index) => {
                                  let inTime = moment(item).format("hh:mm A");
                                  return <td key={index}>{inTime}</td>;
                                })}
                              </tr>
                              <tr className="text-nowrap">
                                <th>Out</th>
                                {data.exit.map((item, index) => {
                                  let outTime = moment(item).format("hh:mm A");
                                  return <td key={index}>{outTime}</td>;
                                })}
                              </tr>
                              <tr className="text-nowrap">
                                <th>Time</th>
                                {data.diffArr.map((item, index) => (
                                  <td key={index}>{item}</td>
                                ))}
                                {/* <td>
                                  {"Effective Time: " +
                                    workingTime(data.effTime)}
                                </td> */}
                              </tr>
                              <tr className="text-nowrap">
                                <th>Effective Time</th>
                                <td colSpan="20">
                                  {workingTime(data.effTime)}
                                </td>
                              </tr>
                            </tbody>
                          </Table>
                        </div>
                      </div>
                    );
                  })
                : !loading && (
                    <div
                      className="position-relative"
                      style={{ minHeight: 121 }}
                    >
                      <div className="no-records">
                        <h5 className="mb-0">
                          <i>No Records Found</i>
                        </h5>
                      </div>
                    </div>
                  )}
            </div>
          </Card>
          {/* <CommonPagination
            activePage={this.state.activePage}
            itemsCountPerPage={2}
            totalItemsCount={450}
            pageRangeDisplayed={5}
            handlePageChange={this.handlePageChange}
          /> */}
        </DefaultLayout>
      </div>
    );
  }
}

export default EmployeeDetail;
