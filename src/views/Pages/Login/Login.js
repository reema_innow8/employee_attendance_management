import React, { Component } from "react";
import User from "../../../assets/image/user.svg";
import Lock from "../../../assets/image/lock.svg";
import Loader from "../../../loader/index";
import { NotificationManager } from "react-notifications";
import {
  Button,
  Card,
  CardBody,
  Col,
  Modal,
  ModalBody,
  FormGroup,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";
import { apiCall } from "../../../common/axios";
import moment from "moment-timezone";
import {
  setWeekOffs,
  setPublicHolidays,
} from "../../../common/changeDataFormat";

const intialstate = {
  email: "",
  password: "",
  loading: false,
  errorType: "",
  errorText: "",
  modal: false,
  emailAddress: "",
};

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = intialstate;
    this.clearError = this.clearError.bind(this);
    this.inputHandler = this.inputHandler.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  clearError() {
    this.setState({
      errorType: "",
      errorText: "",
    });
  }

  inputHandler(e) {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  }
  validateEmail = (email) => {
    var re = /^(([^<>()\]\\.,;:\s@“]+(\.[^<>()\]\\.,;:\s@“]+)*)|(“.+“))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(String(email).toLowerCase());
  };

  handleSubmit() {
    const { email, password } = this.state;
    if (email === "") {
      this.setState({
        errorType: "email",
        errorText: (
          <span className="text-danger">
            <b>Please fill email id</b>
          </span>
        ),
      });
      return;
    }
    if (!this.validateEmail(email)) {
      this.setState({
        errorType: "email",
        errorText: (
          <span className="text-danger">
            <b>Invalid email</b>
          </span>
        ),
      });
      return;
    }
    if (password === "") {
      this.setState({
        errorType: "password",
        errorText: (
          <span className="text-danger">
            <b>Please fill password</b>
          </span>
        ),
      });
      return;
    }
    if (password.length < 6) {
      this.setState({
        errorType: "password",
        errorText: (
          <span className="text-danger">
            <b>Password should be of minimum 6 characters</b>
          </span>
        ),
      });
      return;
    }
    const timeZone = moment.tz.guess(true);
    const loginObj = {
      email,
      password,
      timeZone,
    };
    this.setState({
      loading: true,
    });
    apiCall("auth/login", loginObj)
      .then((res) => {
        const response = res.data;
        if (response.status === 200) {
          localStorage.setItem("isLoggedIn", true);
          const { user } = response.data;
          setWeekOffs(response.data.weekoffs);
          setPublicHolidays(response.data.holidays);
          localStorage.setItem("user", JSON.stringify(user));
          localStorage.setItem("authToken", response.authToken);
          localStorage.setItem("type", user.type);
          localStorage.setItem("empId", user.empId);
          localStorage.setItem("userId", user.id);
          NotificationManager.success(response.message);
          this.setState({
            loading: false,
            email: "",
            password: "",
          });
          this.props.history.push({
            pathname: "/dashboard",
          });
          window.location.reload();
        } else {
          NotificationManager.warning(response.message);
          this.setState({
            loading: false,
            email: "",
            password: "",
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  }
  handlePassword = (e) => {
    this.setState({ modal: !this.state.modal });
  };
  handleSend = () => {
    const { emailAddress } = this.state;

    if (emailAddress === "") {
      this.setState({
        errorType: "emailAddress",
        errorText: (
          <span className="text-danger">
            <b>Please fill email id</b>
          </span>
        ),
      });
      return;
    }
    if (!this.validateEmail(emailAddress)) {
      this.setState({
        errorType: "emailAddress",
        errorText: (
          <span className="text-danger">
            <b>Invalid email</b>
          </span>
        ),
      });
      return;
    }
    const Obj = {
      email: emailAddress,
    };
    this.setState({
      loading: true,
    });
    apiCall("auth/forgot-password", Obj)
      .then((res) => {
        const response = res.data;

        localStorage.setItem("user", JSON.stringify(response.data));
        localStorage.setItem("authToken", response.authToken);

        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.setState({
            loading: false,
            emailAddress: "",
          });
          this.handlePassword();
        } else {
          NotificationManager.warning(response.message);
          this.setState({
            loading: false,
            emailAddress: "",
          });
          this.handlePassword();
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
          this.handlePassword();
        }
      });
  };
  onKeyDown = (e) => {
    if (e.key === "Enter") {
      e.preventDefault();

      this.handleSubmit(e);
    }
  };
  render() {
    const {
      email,
      password,
      errorType,
      errorText,
      loading,
      modal,
      emailAddress,
    } = this.state;

    return (
      <section className="form-section">
        <Container>
          <div className={`f-w-loader ${loading ? "loader" : ""}`}>
            {loading && <Loader />}
            <Card className="login-wrapper">
              <CardBody>
                <Row>
                  <Col md="5">
                    <Form
                      className="login-form"
                      onKeyDown={(e) => this.onKeyDown(e)}
                    >
                      <h1>Welcome,</h1>
                      <p className="text-muted">
                        Please do sign in to proceed.
                      </p>
                      <InputGroup className="mb-3 input-group">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <img src={User} alt="user" height="20px" />
                          </InputGroupText>
                        </InputGroupAddon>

                        <div className="inputWrapper">
                          <Input
                            type="text"
                            name="email"
                            placeholder="Enter email id"
                            onChange={this.inputHandler}
                            value={email}
                          />
                        </div>
                      </InputGroup>

                      {errorType === "email" && (
                        <p className="error">{errorText}</p>
                      )}
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <img src={Lock} alt="lock" height="20px" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <div className="inputWrapper">
                          <Input
                            name="password"
                            type="password"
                            placeholder="Enter Password"
                            onChange={this.inputHandler}
                            value={password}
                            autoComplete="current-password"
                          />
                        </div>
                      </InputGroup>
                      {errorType === "password" && (
                        <p className="error">{errorText}</p>
                      )}
                      <Row className="align-items-center">
                        <Col xs="6">
                          <Button
                            // color="primary"
                            disabled={loading}
                            className="px-4 login-btn theme-bg"
                            onClick={this.handleSubmit}
                          >
                            Login
                            <i className="fas fa-sign-in-alt ml-2" />
                          </Button>
                        </Col>
                        <Col xs="6">
                          <Button
                            color="link"
                            onClick={this.handlePassword}
                            disabled={loading}
                            className="px-0"
                          >
                            Forgot password?
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </Col>
                  <Col md="7">
                    <div className="login-pic"></div>
                  </Col>
                </Row>
              </CardBody>
              <Modal
                isOpen={modal}
                toggle={this.handlePassword}
                className="forgot-modal modal-dialog-centered"
              >
                <ModalBody className={`f-w-loader ${loading ? "loader" : ""}`}>
                  {loading && <Loader />}
                  <div className="crossBtn" onClick={this.handlePassword}>
                    <i className="fas fa-times" />
                  </div>
                  <h5>
                    Please enter your email address, we will send you link to
                    change password
                  </h5>
                  <FormGroup row>
                    <Col md={12}>
                      <Input
                        style={{ maxWidth: "400px", margin: "0 auto" }}
                        onChange={this.inputHandler}
                        type="text"
                        name="emailAddress"
                        id="emailAddress"
                        value={emailAddress}
                        placeholder="Enter email id"
                        disabled={loading}
                      />
                      {errorType === "emailAddress" && <p>{errorText}</p>}
                    </Col>
                  </FormGroup>
                  <div className="text-center sendBtn">
                    <Button
                      color="primary"
                      disabled={loading}
                      onClick={this.handleSend}
                    >
                      Send
                    </Button>
                  </div>{" "}
                  {/* <Button color="secondary" onClick={this.handlePassword}>
                    <i className="fas fa-caret-right mr-2" />
                    Cancel
                  </Button> */}
                </ModalBody>
              </Modal>
            </Card>
          </div>
        </Container>
      </section>
    );
  }
}

export default Login;
