import React, { Component } from "react";
import DefaultLayout from "../../../containers/layout";
import MultipleDatePicker from "../../../components/MultipleDatePicker";
import Tooltip from "../../../components/tooltip";
import {
  Row,
  Modal,
  ModalHeader,
  ModalBody,
  Input,
  Label,
  FormGroup,
  Table,
  Col,
  Card,
  CardHeader,
  Button,
} from "reactstrap";
import Loader from "../../../loader/index";
import { apiCall } from "../../../common/axios";
import moment from "moment";
import arrowup from "../../../assets/image/icons/arrowUp.svg";
import arrowdown from "../../../assets/image/icons/arrowDown.svg";
import {
  dateDifference,
  checkHoliday,
  minHrDayConverter,
} from "../../../common/calculateTime";
import MonthYearPicker from "../../../components/monthYearPicker";
import { NotificationManager } from "react-notifications";
import cx from "classnames";
import { setPublicHolidays } from "../../../common/changeDataFormat";
// import CommonPagination from "../../../common/pagination";
import {
  leavesPerMonth,
  holidayWorkingTimeValue,
  absentTimeValue,
  firstMonthLeaves,
} from "../../../common/credentials";
import { isAdmin } from "../../../common/typeUser";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    const endDate = moment().format("LL");
    const startDate = new Date(new Date(endDate).setDate(1));
    this.state = {
      loading: false,
      startDate: new Date(startDate),
      endDate: new Date(),
      datesArr: this.getDates({ startDate, endDate }),
      employeeDetails: [],
      month: startDate.getMonth() + 1,
      year: startDate.getFullYear(),
      totalEmployees: 0,
      modal: false,
      activePage: 1,
      arrowId: "",
      arrowName: "",
      search: "",
      editModal: false,
      employee_id: "",
      errorText: "",
      errorType: "",
      name: "",
      status_of_employee: "",
      date: [],
      employees: [],
    };
  }

  errorShow = (type) => {
    const { errorType, errorText } = this.state;
    return errorType === type ? <p>{errorText}</p> : null;
  };

  inputHandler = (e) => {
    e.preventDefault();
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };

  calculateHrsValue = (time, isHoliday) => {
    const { days, hours } = time;
    let extra = 0,
      absent = 0;
    if (isHoliday === true) {
      extra = holidayWorkingTimeValue(hours);
    } else if (days <= 0) {
      absent = absentTimeValue(hours);
    }
    return { absent, extra };
  };

  effectiveTime = (data, isHoliday) => {
    let val = 0;
    let { entry, exit } = data;
    entry.sort((a, b) => new Date(a) - new Date(b));
    exit.sort((a, b) => new Date(a) - new Date(b));
    let days = 0,
      hours = 0,
      minutes = 0;
    for (let i = 0; i < entry.length; i++) {
      const startDate = entry[i] || "";
      const endDate = exit[i] || "";
      if (startDate && endDate) {
        let df = dateDifference(startDate, endDate);
        if (df.days) days += df.days;
        if (df.hours) hours += df.hours;
        if (df.minutes) minutes += df.minutes;
      }
    }
    const time = minHrDayConverter({ days, hours, minutes });
    val = this.calculateHrsValue(time, isHoliday);
    return val;
  };

  attendanceCounter = (i) => {
    const { employeeDetails } = this.state;
    let att = 0;
    for (let obj of employeeDetails) {
      if (obj.values[i] && obj.values[i] !== "-" && obj.values[i] !== 1) {
        att++;
      }
    }
    return att;
  };

  clearError = () => {
    this.setState({
      errorType: "",
      errorText: "",
    });
  };
  onChange = (type, str) => {
    let startDate = "",
      endDate = "";
    const { month, year } = this.state;
    const currDate = new Date();
    const currMonth = currDate.getMonth() + 1;
    const currYear = currDate.getFullYear();
    let obj = { month, year };
    if (type === "month") {
      if (year === currYear && str > currMonth) {
        NotificationManager.warning("You can't select this month.");
        return;
      }
      startDate = new Date(year, str - 1, 1);
      endDate = new Date(year, str, 0);
      obj.month = str;
    }
    // for type year
    else {
      obj = {
        month: "",
        year: str,
      };
    }
    if (endDate > currDate) {
      const date = moment().format("LL");
      startDate = new Date(new Date(date).setDate(1));
      endDate = new Date();
    }
    if (str === currYear) {
      window.location.reload();
    }
    obj = {
      ...obj,
      startDate,
      endDate,
    };
    if (obj.month && obj.year) {
      this.fetch(obj);
    } else {
      this.setState(obj);
    }
    if (str === obj.month) {
      this.toggle();
    }
  };

  componentDidMount() {
    const obj = {
      startDate: this.state.startDate,
      endDate: this.state.endDate,
      month: this.state.month,
      year: this.state.year,
    };
    this.fetch(obj);
  }

  // static getDerivedStateFromProps(props, state) {
  //   const obj = {
  //     startDate: state.startDate,
  //     endDate: state.endDate,
  //     month: state.month,
  //     year: state.year,
  //   };

  //   fetch(obj);
  // }

  prevLeaves = (employee) => {
    let bal = 0;
    const { month, year } = this.state;
    const { joiningDate, leaveBalance } = employee;
    const selectedDate = moment(`${year}-${month}`).format();
    if (joiningDate) {
      const joinDate = moment(joiningDate);
      if (joinDate.format() > selectedDate) {
        bal = firstMonthLeaves(parseInt(joinDate.format("D")));
      } else {
        bal = leavesPerMonth;
      }
    }
    if (leaveBalance && leaveBalance.length > 0) {
      const firstDateOfCurYear = moment().startOf("year").format();
      for (let item of leaveBalance) {
        const date = moment(item.date).format();
        if (firstDateOfCurYear <= date && date < selectedDate) {
          if (item.count.balance > 0) {
            bal += item.count.balance;
          }
        }
      }
    }
    return bal;
  };

  checkJoiningDate = (employee, date) => {
    let isJoined = true;
    const { joiningDate } = employee;
    if (joiningDate) {
      isJoined = moment(joiningDate).format() <= date.format();
    }
    return isJoined;
  };

  calculateTimeLeaves = (entries) => {
    const { datesArr } = this.state;
    let effHrs = [];
    for (let entry of entries) {
      const employee =
        (entry.employee && entry.employee.length > 0 && entry.employee[0]) ||
        "";
      let isJoined = this.checkJoiningDate(
        employee,
        datesArr[datesArr.length - 1]
      );
      if (
        employee &&
        entry.list &&
        entry.list.length > 0 &&
        isJoined === true
      ) {
        let obj = {
          id: employee.empId || "",
          name: employee.name || "",
          prevLeaves: this.prevLeaves(employee),
        };
        let absent = 0;
        let extra = 0;
        let lop = 0;
        const values = [];
        for (let date of datesArr) {
          let text = "-";
          isJoined = this.checkJoiningDate(employee, date);
          if (isJoined === true) {
            let val = { extra: 0, absent: 1 };
            const onlyDate = date.format("DD-MM-YYYY");
            const index = entry.list.findIndex((i) => i.date === onlyDate);
            const isHoliday = checkHoliday(
              date.format("ddd"),
              date.format("DD MMM")
            );
            if (index > -1) {
              val = this.effectiveTime(entry.list[index], isHoliday);
            } else if (isHoliday === true) {
              val.absent = 0;
            }
            if (isHoliday === false && val.absent > 0) {
              const index = entry.leaves.findIndex((i) => {
                const ind = i.dates.findIndex((j) => {
                  const k = moment(j).format();
                  if (k === date.format()) return true;
                  return null;
                });
                if (ind > -1) return true;
                return null;
              });
              if (index < 0) {
                lop += val.absent;
                if (val.absent === 1) {
                  text = "LOP";
                }
              } else absent += val.absent;
            }
            extra += val.extra;
            if (val.absent > 0) {
              if (text !== "LOP") {
                text = val.absent;
              }
            } else if (val.extra > 0) {
              text = -val.extra;
            } else if (isHoliday === false) {
              text = "P";
            }
          }
          values.push(text);
        }
        effHrs.push({ ...obj, leaves: absent, extra, lop, values });
      }
    }
    return effHrs;
  };

  fetch = (data) => {
    const arrayDates = this.getDates({
      startDate: data && data.startDate,
      endDate: data && data.endDate,
    });
    this.setState({
      loading: true,
      ...data,
      datesArr: arrayDates,
    });
    const user = JSON.parse(localStorage.getItem("user"));
    const obj = {
      id: (user && user.id) || "",
      from: data && data.startDate,
      to: new Date(
        data && new Date(data.endDate).setDate(data.endDate.getDate() + 1)
      ),
    };
    apiCall("attendance/fetch-all", obj)
      .then((res) => {
        const { data } = res.data;
        if (res.data.status === 200) {
          setPublicHolidays(data.holidays);
          let arr = this.calculateTimeLeaves(data.attendance);
          arr = this.sortRecords("up", "", arr);
          const empObj = { totalEmployees: 0, employees: [] };
          if (data && data.employees && data.employees.length > 0) {
            empObj.totalEmployees = data.employees.length;
            empObj.employees = data.employees;
          }
          this.setState({
            loading: false,
            employeeDetails: arr,
            ...empObj,
          });
        } else {
          this.setState({
            loading: false,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };

  getDates = (dates) => {
    let start = "",
      end = "";
    if (dates) {
      start = moment(dates.startDate).format();
      end = moment(dates.endDate).format();
    } else {
      start = moment(this.state.startDate).format();
      end = moment(this.state.endDate).format();
    }
    let arr = [];
    for (; start <= end; start = moment(start).add(1, "days").format()) {
      arr.push(moment(start));
    }
    return arr;
  };

  // handlePageChange = (pageNumber) => {
  //   this.setState({ activePage: pageNumber });
  // };

  sortRecords = (order, sortBy, records) => {
    const { employeeDetails } = this.state;
    if (sortBy) {
      if (order === "up") {
        employeeDetails.sort((a, b) => {
          const valA = a[sortBy].toLowerCase();
          const valB = b[sortBy].toLowerCase();
          if (valA > valB) {
            return 1;
          } else if (valA < valB) {
            return -1;
          } else {
            return 0;
          }
        });
      } else if (order === "down") {
        employeeDetails.sort((a, b) => {
          const valA = a[sortBy].toLowerCase();
          const valB = b[sortBy].toLowerCase();
          if (valA > valB) {
            return -1;
          } else if (valA < valB) {
            return 1;
          } else {
            return 0;
          }
        });
      }
    } else {
      if (records && records.length > 0) {
        return records.sort((a, b) => {
          const valA = (a.id + a.name).toLowerCase();
          const valB = (b.id + b.name).toLowerCase();
          if (valA > valB) {
            return 1;
          } else if (valA < valB) {
            return -1;
          } else {
            return 0;
          }
        });
      }
    }
  };

  handleUpDown = (type, sortBy) => {
    if (sortBy === "arrowId") {
      this.sortRecords(type, "id");
    } else {
      this.sortRecords(type, "name");
    }
    this.setState({
      [sortBy]: type,
    });
  };

  filterRecords = () => {
    const { employeeDetails } = this.state;
    const search = this.state.search.trim().replace(/ +/g, " ");

    if (!search) return employeeDetails;
    return (
      employeeDetails &&
      employeeDetails.filter((data) => {
        let isTrue =
          data.id.toLowerCase().includes(search) ||
          data.name.toLowerCase().includes(search);
        return isTrue;
      })
    );
  };

  editDashboard = (e) => {
    this.clearError();
    this.setState({
      editModal: !this.state.editModal,
      status_of_employee: "",
      employee_id: "",
      name: "",
    });
  };

  handleSubmit = () => {
    const {
      employee_id,
      status_of_employee,
      date,
      startDate,
      endDate,
      name,
    } = this.state;
    if (employee_id === "") {
      this.setState({
        errorType: "employee_id",
        errorText: (
          <span className="text-danger">
            <b>Employee id should not be empty</b>
          </span>
        ),
      });
      return false;
    }
    if (employee_id !== "") {
      var filter = /^[a-zA-Z0-9]+([-_]{1}[a-zA-Z0-9]+)*$/;
      if (!filter.test(employee_id)) {
        this.setState({
          errorType: "employee_id",
          errorText: (
            <span className="text-danger">
              <b>
                Id should be combination of alphanumeric text, underscore and
                hyphen
              </b>
            </span>
          ),
        });
        return;
      }
    }

    // if (name === "") {
    //   this.setState({
    //     errorType: "name",
    //     errorText: (
    //       <span className="text-danger">
    //         <b>Name should not be empty</b>
    //       </span>
    //     ),
    //   });
    //   return;
    // }
    // if (name !== "") {
    //   let filter = /^[a-zA-Z0-9]+([-_\s]{1}[a-zA-Z0-9]+)*$/;

    //   if (!filter.test(name)) {
    //     this.setState({
    //       errorType: "name",
    //       errorText: (
    //         <span className="text-danger">
    //           <b>
    //             {" "}
    //             Name should be combination of alphanumeric text, underscore,
    //             space and hyphen
    //           </b>
    //         </span>
    //       ),
    //     });
    //     return;
    //   }
    // }
    if (date && date.length === 0) {
      this.setState({
        errorType: "date",
        errorText: (
          <span className="text-danger">
            <b>Please select the date</b>
          </span>
        ),
      });
      return false;
    }

    if (status_of_employee === "") {
      this.setState({
        errorType: "status_of_employee",
        errorText: (
          <span className="text-danger">
            <b>Please select status</b>
          </span>
        ),
      });
      return false;
    }
    this.setState({
      loading: true,
    });
    const userId = JSON.parse(localStorage.getItem("user")).id;
    const editObj = {
      id: userId,
      empId: employee_id,
      dates: date[date.length - 1].dates,
      status: status_of_employee,
      name: name,
    };
    apiCall("attendance/edit", editObj)
      .then((res) => {
        const response = res.data;

        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.fetch({ startDate, endDate });
        } else {
          NotificationManager.warning(response.message);
        }
        this.setState({
          loading: false,
          editModal: false,
        });
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
            editModal: false,
          });
        }
      });
  };
  clearError = () => {
    this.setState({
      errorType: "",
      errorText: "",
    });
  };
  onSubmit = (dates) => {
    this.clearError();
    let arr = this.state.date;
    arr.push({
      dates,
    });
    this.setState({
      date: arr,
    });
  };

  checkColor = (text) => {
    switch (text) {
      case "P":
      case -1:
        return "green";
      case 0.25:
      case -0.75:
        return "gold";
      case 0.5:
      case -0.5:
        return "orange";
      default:
        return "red";
    }
  };

  render() {
    const {
      datesArr,
      loading,
      month,
      year,
      totalEmployees,
      arrowId,
      arrowName,
      editModal,
      employee_id,

      status_of_employee,
      employees,
    } = this.state;
    const searchRecords = this.filterRecords() || [];

    // const type = localStorage.getItem("type");
    const editAtdEmpInd = employees.findIndex((i) => i.empId === employee_id);

    const selectedMonth =
      (month &&
        moment()
          .month(month - 1)
          .format("MMMM")) ||
      "";
    return (
      <div>
        <DefaultLayout>
          <Card>
            <Row>
              <Col xs="12">
                <CardHeader>
                  <div className="text-center">
                    <div className="dashHeader">
                      <h2 className="picker-title">Select month and year</h2>

                      {isAdmin() && (
                        <Button
                          id="edit_status"
                          className="ml-2 dashboardEdit"
                          color="primary"
                          onClick={this.editDashboard}
                          disabled={loading}
                        >
                          <i className="fas fa-edit"></i>
                        </Button>
                      )}
                      {searchRecords && searchRecords.length > 0 && (
                        <div className="notes-hint">
                          <ul className="pl-0">
                            <li>
                              <span className="green"></span> P for Present, -1
                              for full day compensation
                            </li>
                            <li>
                              <span className="red"></span> 1 for Absent, LOP
                              for Loss of Pay
                            </li>
                            <li>
                              <span className="orange"></span> 0.5 for Half Day
                            </li>
                            <li>
                              <span className="yellow"></span> 0.25 for Short
                              leave
                            </li>
                          </ul>
                        </div>
                      )}
                    </div>
                    {isAdmin() && (
                      <Tooltip placement="left" target="edit_status">
                        Edit
                      </Tooltip>
                    )}
                    <Button
                      disabled={loading}
                      onClick={this.toggle}
                      color="primary"
                      className="btnDate-picker mb-3"
                    >
                      {selectedMonth && year
                        ? `${selectedMonth}, ${year}`
                        : "Select Month"}
                      {/* {currYear && currMonth
                        ? "Select Month And Year"
                        : `${selectedMonth}, ${year}`} */}
                    </Button>
                    <Modal
                      isOpen={editModal}
                      toggle={this.editDashboard}
                      className="leaveModal mt-5"
                    >
                      <ModalHeader>Manage Attendance Manually</ModalHeader>
                      <ModalBody
                        className={`f-w-loader ${loading ? "loader" : ""}`}
                      >
                        {loading && <Loader />}
                        <div>
                          <FormGroup row>
                            <Col md={3}>
                              <Label for="emp id">Emp Id</Label>
                              <span className="text-danger">*</span>
                            </Col>

                            <Col md={9}>
                              <Input
                                style={{ maxWidth: "initial" }}
                                type="text"
                                onChange={this.inputHandler}
                                name="employee_id"
                                value={employee_id}
                                placeholder="Enter employee id"
                                disabled={loading}
                              />
                              {this.errorShow("employee_id")}
                            </Col>
                          </FormGroup>
                          {employee_id && editAtdEmpInd > -1 && (
                            <FormGroup row>
                              <Col md={3}>
                                <Label for="name">Name</Label>
                                <span className="text-danger">*</span>
                              </Col>

                              <Col md={9}>
                                <Input
                                  style={{ maxWidth: "initial" }}
                                  type="text"
                                  onChange={this.inputHandler}
                                  name="name"
                                  disabled
                                  value={
                                    editAtdEmpInd > -1
                                      ? employees[editAtdEmpInd].name
                                      : ""
                                  }
                                  placeholder="Enter name"
                                />
                                {this.errorShow("name")}
                              </Col>
                            </FormGroup>
                          )}
                          <FormGroup row>
                            <Col md={3} className="text-left">
                              <Label for="date" className="mr-2 mb-0">
                                <b>Date</b>
                                <span className="text-danger">*</span>
                              </Label>
                            </Col>
                            <Col md={9} className="date">
                              <MultipleDatePicker
                                style={{ maxWidth: "initial" }}
                                onSubmit={this.onSubmit}
                                name="date"
                                id="date"
                                disabled={loading}
                                // minDate={new Date()}
                              />
                              {this.errorShow("date")}
                            </Col>
                          </FormGroup>

                          {/* <FormGroup row>
                            <Col md={3}>
                              <Label for="name">Name:</Label>
                            </Col>

                            <Col md={9}>
                              <Input
                                style={{ maxWidth: "initial" }}
                                type="text"
                                placeholder="Enter name"
                                onChange={this.inputHandler}
                                name="name"
                                value={name}
                              />
                              {this.errorShow("name")}
                            </Col>
                          </FormGroup> */}

                          <FormGroup row>
                            <Col md={3}>
                              <Label for="status_of_employee">Status</Label>
                              <span className="text-danger">*</span>
                            </Col>

                            <Col md={9}>
                              <Input
                                style={{ maxWidth: "initial" }}
                                type="select"
                                onChange={this.inputHandler}
                                name="status_of_employee"
                                id="status_of_employee"
                                value={status_of_employee}
                                disabled={loading}
                              >
                                <option value="select">Select</option>
                                <option value="present">Present</option>
                                <option value="half_day">Half Day</option>
                                <option value="short_leave">Short Leave</option>
                                <option value="absent">Absent or LOP</option>
                              </Input>
                              {this.errorShow("status_of_employee")}
                            </Col>
                          </FormGroup>

                          <div className="text-right dashSubBtn pt-2">
                            <Button
                              color="primary"
                              onClick={this.handleSubmit}
                              disabled={loading}
                            >
                              Submit
                            </Button>
                          </div>
                        </div>
                      </ModalBody>
                    </Modal>
                    <Modal
                      isOpen={this.state.modal}
                      toggle={this.toggle}
                      className="modal-dialog-centered month-picker-modal"
                    >
                      {/* <ModalHeader toggle={this.toggle}></ModalHeader> */}

                      <div>
                        <MonthYearPicker
                          selectedMonth={month}
                          selectedYear={year}
                          minYear={2000}
                          maxYear={new Date().getFullYear()}
                          onChangeYear={(y) => this.onChange("year", y)}
                          onChangeMonth={(m) => this.onChange("month", m)}
                        />
                      </div>
                    </Modal>

                    {isAdmin() && (
                      <div>
                        <input
                          type="search"
                          placeholder="Search by an employee name or an id"
                          required
                          onChange={(e) =>
                            this.setState({ search: e.target.value })
                          }
                          className="search"
                        />
                      </div>
                    )}
                  </div>
                </CardHeader>
              </Col>
            </Row>
            <div
              className={`dashboard tableWrapper f-w-loader ${
                loading ? "loader" : ""
              }`}
            >
              {searchRecords && searchRecords.length > 0 && (
                <>
                  <Table
                    responsive
                    striped
                    className="emp-details text-center mb-0"
                  >
                    <thead>
                      <tr>
                        <th className="text-nowrap p-0">
                          <div
                            className="d-flex"
                            style={{
                              height: 52,
                              borderBottom: "1px solid #d7e0fc",
                            }}
                          >
                            <span className="static empId">
                              Emp Id{" "}
                              {isAdmin() && (
                                <span className="ml-2">
                                  {arrowId === "down" ? (
                                    <img
                                      alt=""
                                      style={{ cursor: "pointer" }}
                                      onClick={(e) =>
                                        this.handleUpDown("up", "arrowId")
                                      }
                                      src={arrowdown}
                                    />
                                  ) : (
                                    <img
                                      alt=""
                                      style={{ cursor: "pointer" }}
                                      onClick={(e) =>
                                        this.handleUpDown("down", "arrowId")
                                      }
                                      src={arrowup}
                                    />
                                  )}
                                </span>
                              )}
                            </span>

                            <span className="static empName">
                              Name
                              {isAdmin() && (
                                <span className="ml-2">
                                  {arrowName === "down" ? (
                                    <img
                                      alt=""
                                      style={{ cursor: "pointer" }}
                                      onClick={(e) =>
                                        this.handleUpDown("up", "arrowName")
                                      }
                                      src={arrowdown}
                                    />
                                  ) : (
                                    <img
                                      alt=""
                                      style={{ cursor: "pointer" }}
                                      onClick={(e) =>
                                        this.handleUpDown("down", "arrowName")
                                      }
                                      src={arrowup}
                                    />
                                  )}
                                </span>
                              )}
                            </span>
                          </div>
                        </th>
                        {searchRecords &&
                          searchRecords.length > 0 &&
                          datesArr &&
                          datesArr.length > 0 &&
                          datesArr.map((data, index) => {
                            let day = "",
                              date = "";
                            if (data) {
                              day = data.format("ddd");
                              date = data.format("DD MMM");
                            }
                            return (
                              <th
                                key={index}
                                className={`text-nowrap ${
                                  day ? "weekend" : ""
                                } `}
                                style={{ lineHeight: 1 }}
                              >
                                {date} <br />
                                <span
                                  style={{
                                    fontSize: "80%",
                                    fontWeight: 400,
                                  }}
                                >
                                  {day}
                                </span>
                              </th>
                            );
                          })}
                        <th className="text-nowrap p-0">
                          <div
                            className="d-flex"
                            style={{
                              height: 52,
                              borderBottom: "1px solid #d7e0fc",
                            }}
                          >
                            <span className="static">Leaves</span>
                            {/* <span className="static darkBlue"> */}
                            <span className="static">
                              Leave Bal <br />
                              on {datesArr && datesArr[0].format("DD MMM")}{" "}
                            </span>
                            <span className="static">
                              Leave Bal <br />
                              on{" "}
                              {datesArr &&
                                datesArr[datesArr.length - 1].format(
                                  "DD MMM"
                                )}{" "}
                            </span>
                            {/* <span className="static">
                              LOP
                              <br />
                              (Loss Of Pay)
                            </span> */}
                          </div>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
                      {searchRecords &&
                        searchRecords.length > 0 &&
                        searchRecords.map((data, index) => {
                          let name = "",
                            id = "";
                          if (data) {
                            name = data.name || "";
                            id = data.id || "";
                          }
                          const leaves = data.leaves - data.extra;
                          const initialBal = data.prevLeaves;
                          const finalBal = data.prevLeaves - leaves;
                          return (
                            <tr key={index}>
                              <td className="p-0">
                                <div className="d-flex">
                                  <span className="static empId">{id}</span>
                                  <span className="static empName">{name}</span>
                                </div>
                              </td>

                              {data &&
                                data.values &&
                                data.values.length > 0 &&
                                data.values.map((i, index) => {
                                  const dateObj = datesArr[index];
                                  let day = "",
                                    date = "";
                                  if (dateObj) {
                                    day = dateObj.format("ddd");
                                    date = dateObj.format("DD MMM");
                                  }
                                  return (
                                    <td
                                      key={index}
                                      className={cx("text-nowrap", {
                                        highlighted: checkHoliday(day, date),
                                      })}
                                    >
                                      <span
                                        style={{ color: this.checkColor(i) }}
                                      >
                                        {i}
                                      </span>
                                    </td>
                                  );
                                })}
                              <td key={index} className="p-0">
                                <div className="d-flex">
                                  <span className="static">{leaves}</span>
                                  {/* <span className="static darkBlueAbsolute"> */}
                                  <span className="static">{initialBal}</span>
                                  <span className="static">{finalBal}</span>
                                  {/* <span className="static">{data.lop}</span> */}
                                </div>
                              </td>
                            </tr>
                          );
                          // }
                        })}
                      {isAdmin() && (
                        <tr>
                          <td style={{ background: "#fff" }}>
                            {" "}
                            <h5 className="mb-0">
                              <label className="mb-0">Present/Total</label>
                            </h5>
                          </td>
                          {searchRecords &&
                            searchRecords.length > 0 &&
                            datesArr &&
                            datesArr.length > 0 &&
                            datesArr.map((data, index) => {
                              const attendance = this.attendanceCounter(index);
                              return (
                                <td key={index} style={{ background: "#fff" }}>
                                  {attendance}/{totalEmployees}
                                </td>
                              );
                            })}
                          <td style={{ background: "#fff" }} />
                        </tr>
                      )}
                    </tbody>
                  </Table>
                  {/* <CardBody>
                    <h5 className="text-right total-value mb-0">
                      <label className="mb-0">Present/Total</label> = 10/40
                    </h5>
                  </CardBody> */}
                </>
              )}

              {loading ? (
                <Loader />
              ) : (
                searchRecords &&
                searchRecords.length === 0 && (
                  <div className="position-relative" style={{ minHeight: 106 }}>
                    <div className="no-records">
                      <h5 className="mb-0">
                        <i>
                          {" "}
                          No Records Found{" "}
                          {selectedMonth ? `For ${selectedMonth} Month` : ""}
                        </i>
                      </h5>
                    </div>
                  </div>
                )
              )}
            </div>
          </Card>
          {/* <CommonPagination
          //   activePage={this.state.activePage}
          //   itemsCountPerPage={2}
          //   totalItemsCount={450}
          //   pageRangeDisplayed={5}
          //   handlePageChange={this.handlePageChange}
          // /> */}
          {/* {searchRecords && searchRecords.length !== 0 && (
            <div className="notes">
              <span>
                <strong className="mr-2">Note 1 (Working Days):</strong> */}
          {/* Symbol "P" for Present, "1" for Absent, "-" for Public Holiday or
              Week-Off, "-1" for Extra working Day, "FD" for Full Day Leave,
              "HD" for Half Day Leave, "SL" for Short Day Leave */}
          {/* Symbol "P" for Present, "-" for Public Holiday or Week-Off,
                "+ve" value for Absent, "-ve" value for Extra Working Time, "1"
                for Full Day, "0.5" for Half Day, "0.25" for 2 Hrs, "FD" for
                Full Day Leave, "HD" for Half Day Leave, "SL" for Short Day
                Leave */}
          {/* Symbol "P" for Present, "-" for Public Holiday or Week-Off,
                "+ve" value for Absent, "-ve" value for Extra Working Time, "1"
                for Full Day, "0.5" for Half Day, "0.25" for Short Leave */}
          {/* Symbol "P" for Present, "1" value for Absent, "0.5" for Half Day,
          "0.25" for Short Leave, Symbol "LOP" for Absent without information */}
          {/* </span>
              <span>
                <strong className="mr-2">
                  Note 2 (Week-Offs/Public Holidays/Extra Working Days):
                </strong> */}
          {/* Symbol "P" for Present, "1" for Absent, "-" for Public Holiday or
              Week-Off, "-1" for Extra working Day, "FD" for Full Day Leave,
              "HD" for Half Day Leave, "SL" for Short Day Leave */}
          {/* Symbol "P" for Present, "-" for Public Holiday or Week-Off,
                "+ve" value for Absent, "-ve" value for Extra Working Time, "1"
                for Full Day, "0.5" for Half Day, "0.25" for 2 Hrs, "FD" for
                Full Day Leave, "HD" for Half Day Leave, "SL" for Short Day
                Leave */}
          {/* Symbol "P" for Present, "-" for Public Holiday or Week-Off,
                "+ve" value for Absent, "-ve" value for Extra Working Time, "1"
                for Full Day, "0.5" for Half Day, "0.25" for Short Leave */}
          {/* "-1" value for more than 9 hours, "0.5" for more than 5 hours
                and less than 7 hours, "0.75" for less than 9 hours and more
                than 7 hours, "-" for Public Holiday or Week-Off
              </span>
            </div>
          )} */}
        </DefaultLayout>
      </div>
    );
  }
}

export default Dashboard;
