import React, { Component } from "react";
import DefaultLayout from "../../../containers/layout/index";
import DatePicker from "react-datepicker";
import {
  Card,
  CardHeader,
  CardBody,
  Button,
  Form,
  FormGroup,
  Input,
  Label,
  Col,
} from "reactstrap";
import Loader from "../../../loader/index";
import { apiCall } from "../../../common/axios";
import { NotificationManager } from "react-notifications";
import moment from "moment-timezone";

const validateEmail = (email) => {
  var re = /^(([^<>()\]\\.,;:\s@“]+(\.[^<>()\]\\.,;:\s@“]+)*)|(“.+“))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

class AddEmployee extends Component {
  constructor(props) {
    super(props);
    const employeesArray =
      (this.props && this.props.history.location.state) || {};
    this.state = {
      employee_id: employeesArray.empId || "",
      name: employeesArray.name || "",
      email: employeesArray.email || "",
      contact: employeesArray.contact || "",
      startDate: employeesArray.joiningDate || "",
      loading: false,
      errorType: "",
      errorText: "",
    };
  }
  handleStartDate = (date) => {
    this.clearError();
    date = new Date(date);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    this.setState({
      startDate: date,
    });
    this.clearError();
  };
  clearError = () => {
    this.setState({
      errorType: "",
      errorText: "",
    });
  };

  inputHandler = (e) => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };
  updateEmployee = (e) => {
    const { name, contact, employee_id, email, startDate } = this.state;
    // const user_details = localStorage.getItem("user");
    this.setState({
      loading: true,
    });
    const employeeObj = {
      name,
      email,
      contact,
      empId: employee_id,
      id: this.props.history.location.state._id,
      joiningDate: startDate,
    };
    apiCall("employee/edit", employeeObj)
      .then((res) => {
        const response = res.data;

        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.setState({
            loading: false,
          });
          this.props.history.push("/employees");
        } else {
          NotificationManager.warning(response.message);
          this.setState({
            loading: false,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };
  addEmployee = (e) => {
    const { name, contact, employee_id, email, startDate } = this.state;
    const user_details = localStorage.getItem("user");
    this.setState({
      loading: true,
    });
    const timeZone = moment.tz.guess(true);
    const employeeObj = {
      name,
      email,
      contact,
      empId: employee_id,
      userId: user_details && JSON.parse(user_details).id,
      timeZone,
      joiningDate: startDate,
    };

    apiCall("employee/add", employeeObj)
      .then((res) => {
        const response = res.data;
        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.setState({
            loading: false,
          });
          this.props.history.push("/employees");
        } else {
          NotificationManager.warning(response.message);
          this.setState({
            loading: false,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };

  handleSubmit = (e) => {
    const { name, email, contact, employee_id } = this.state;
    const employeesArray =
      (this.props && this.props.history.location.state) || {};
    if (employee_id === "") {
      this.setState({
        errorType: "employee_id",
        errorText: (
          <span className="text-danger">
            <b>Employee id should not be empty</b>
          </span>
        ),
      });
      return false;
    }
    if (employee_id !== "") {
      var filter = /^[a-zA-Z0-9]+([-_]{1}[a-zA-Z0-9]+)*$/;
      if (!filter.test(employee_id)) {
        this.setState({
          errorType: "employee_id",
          errorText: (
            <span className="text-danger">
              <b>
                Id should be combination of alphanumeric text, underscore and
                hyphen
              </b>
            </span>
          ),
        });
        return;
      }
    }
    // if (startDate === "") {
    //   this.setState({
    //     errorType: "startDate",
    //     errorText: (
    //       <span className="text-danger">
    //         <b>Please enter the joining date</b>
    //       </span>
    //     ),
    //   });
    //   return false;
    // }
    if (name === "") {
      this.setState({
        errorType: "name",
        errorText: (
          <span className="text-danger">
            <b>Name should not be empty</b>
          </span>
        ),
      });
      return;
    }
    if (name !== "") {
      let filter = /^[a-zA-Z0-9]+([-_\s]{1}[a-zA-Z0-9]+)*$/;

      if (!filter.test(name)) {
        this.setState({
          errorType: "name",
          errorText: (
            <span className="text-danger">
              <b>
                {" "}
                Name should be combination of alphanumeric text, underscore,
                space and hyphen
              </b>
            </span>
          ),
        });
        return;
      }
    }

    if (email === "") {
      this.setState({
        errorType: "email",
        errorText: (
          <span className="text-danger">
            <b>Email should not be empty</b>
          </span>
        ),
      });
      return false;
    }
    if (!validateEmail(email)) {
      this.setState({
        errorType: "email",
        errorText: (
          <span className="text-danger">
            <b>Invalid email</b>
          </span>
        ),
      });
      return false;
    }
    if (contact === "") {
      this.setState({
        errorType: "contact",
        errorText: (
          <span className="text-danger">
            <b>Contact should not be empty</b>
          </span>
        ),
      });
      return;
    } else if (contact.length < 10 || contact.length > 10) {
      this.setState({
        errorType: "contact",
        errorText: (
          <span className="text-danger">
            <b>Contact should be of length 10</b>
          </span>
        ),
      });
      return;
    } else if (contact !== "") {
      let filter = /^\d{10}$/;
      if (!filter.test(contact)) {
        this.setState({
          errorType: "contact",
          errorText: (
            <span className="text-danger">
              <b>Please enter valid contact number</b>
            </span>
          ),
        });
        return;
      }
    }

    if (employeesArray && Object.keys(employeesArray).length > 0) {
      this.updateEmployee();
    } else {
      this.addEmployee();
    }
  };

  errorShow = (type) => {
    const { errorType, errorText } = this.state;
    return errorType === type ? <p>{errorText}</p> : null;
  };

  handleBack = (e) => {
    e.preventDefault();
    this.props.history.push("/employees");
  };

  resetState = (e) => {
    e.preventDefault();
    this.setState({
      employee_id: "",
      startDate: "",
      name: "",
      email: "",
      contact: "",
      errorText: "",
      errorType: "",
    });
  };

  render() {
    const { name, email, contact, employee_id, loading } = this.state;

    return (
      <div>
        <DefaultLayout>
          <Form>
            <Card>
              <CardHeader>
                <Button
                  disabled={loading}
                  onClick={this.handleBack}
                  className="backBtn"
                >
                  <i className="fas fa-arrow-left"></i>Back
                </Button>
              </CardHeader>
              <CardBody className={`f-w-loader ${loading ? "loader" : ""}`}>
                {loading && <Loader />}
                <div className="loader-content-wrapper">
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="employee id">
                        <b>Employee Id</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="text"
                        onChange={this.inputHandler}
                        name="employee_id"
                        value={employee_id}
                        placeholder="Enter employee id"
                      />
                      {this.errorShow("employee_id")}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="date of joining">
                        <b>Date Of Joining</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <div className="datePikkerIconWrapper noIcon datepickerdisableWeekend">
                        <DatePicker
                          selected={
                            this.state.startDate
                              ? new Date(this.state.startDate)
                              : ""
                          }
                          onChange={this.handleStartDate}
                          dateFormat="dd/MM/yyyy"
                          placeholderText="Enter joining date"
                        />
                        {/* <i className="far fa-calendar-alt text-primary" /> */}

                        {this.errorShow("startDate")}
                      </div>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="name">
                        <b>Name</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="text"
                        placeholder="Enter name"
                        onChange={this.inputHandler}
                        name="name"
                        value={name}
                      />
                      {this.errorShow("name")}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="Email">
                        <b>Email</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="email"
                        placeholder="Enter email"
                        onChange={this.inputHandler}
                        name="email"
                        value={email}
                      />
                      {this.errorShow("email")}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="contact">
                        <b>Contact</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="text"
                        // pattern="[789][0-9]{9}"
                        placeholder="Enter your contact number"
                        onChange={this.inputHandler}
                        name="contact"
                        value={contact}
                      />
                      {this.errorShow("contact")}
                    </Col>
                  </FormGroup>

                  <div className="ml-auto mr-0 w-fit-content text-right">
                    <Button
                      type="button"
                      color="success"
                      onClick={this.handleSubmit}
                      disabled={loading}
                    >
                      <i className="fa fa-dot-circle-o" /> Submit
                    </Button>
                    <Button
                      className="ml-3"
                      type="reset"
                      color="danger"
                      disabled={loading}
                      onClick={this.resetState}
                    >
                      <i className="fa fa-ban" /> Reset
                    </Button>
                  </div>
                </div>
              </CardBody>
            </Card>
          </Form>
        </DefaultLayout>
      </div>
    );
  }
}
export default AddEmployee;
