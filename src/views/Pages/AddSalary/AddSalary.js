import React, { Component } from "react";
import DefaultLayout from "../../../containers/layout/index";
import {
  Card,
  CardHeader,
  CardBody,
  Button,
  Form,
  FormGroup,
  Input,
  Label,
  Col,
} from "reactstrap";
import Loader from "../../../loader/index";
import { apiCall } from "../../../common/axios";
import { NotificationManager } from "react-notifications";
import moment from "moment-timezone";

// const validateEmail = (email) => {
//   var re = /^(([^<>()\[\]\\.,;:\s@“]+(\.[^<>()\[\]\\.,;:\s@“]+)*)|(“.+“))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//   return re.test(String(email).toLowerCase());
// };

class AddSalary extends Component {
  constructor(props) {
    super(props);
    const employeesArray =
      (this.props && this.props.history.location.state) || {};

    this.state = {
      employee_id:
        (employeesArray &&
          employeesArray.employee &&
          employeesArray.employee.empId) ||
        "",
      // name:
      //   (employeesArray &&
      //     employeesArray.employee &&
      //     employeesArray.employee.name) ||
      //   "",

      salary:
        (employeesArray && employeesArray.employee && employeesArray.salary) ||
        "",
      loading: false,
      errorType: "",
      errorText: "",
    };
  }
  clearError = () => {
    this.setState({
      errorType: "",
      errorText: "",
    });
  };

  inputHandler = (e) => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };
  updateSalary = (e) => {
    const { salary } = this.state;
    // const user_details = localStorage.getItem("user");
    this.setState({
      loading: true,
    });
    const salaryObj = {
      // name,

      salary,
      // empId: employee_id,
      id: this.props.history.location.state._id,
    };
    apiCall("salary/edit", salaryObj)
      .then((res) => {
        const response = res.data;

        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.setState({
            loading: false,
          });
          this.props.history.push("/salary");
        } else {
          NotificationManager.warning(response.message);
          this.setState({
            loading: false,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };
  addSalary = (e) => {
    const { employee_id, salary } = this.state;
    const user_details = localStorage.getItem("user");
    this.setState({
      loading: true,
    });
    const timeZone = moment.tz.guess(true);
    const salaryObj = {
      // name,

      salary,
      id: employee_id,
      userId: user_details && JSON.parse(user_details).id,
      timeZone,
    };
    apiCall("salary/add", salaryObj)
      .then((res) => {
        const response = res.data;

        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.setState({
            loading: false,
          });
          this.props.history.push("/salary");
        } else if (response.status === 400) {
          // NotificationManager.warning("Please enter the correct id");
          // this.setState({
          //   loading: false,
          // });
          this.setState({
            loading: false,
            errorType: "employee_id",
            errorText: (
              <span className="text-danger">
                <b>Incorrect Id</b>
              </span>
            ),
          });
          return false;
        } else {
          NotificationManager.warning("Salary has been already added");
          this.setState({
            loading: false,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };

  handleSubmit = (e) => {
    const { employee_id, salary } = this.state;
    const employeesArray =
      (this.props && this.props.history.location.state) || {};

    if (employee_id === "") {
      this.setState({
        errorType: "employee_id",
        errorText: (
          <span className="text-danger">
            <b>Employee id should not be empty</b>
          </span>
        ),
      });
      return false;
    }
    if (employee_id !== "") {
      var filter = /^[a-zA-Z0-9]+([-_]{1}[a-zA-Z0-9]+)*$/;
      if (!filter.test(employee_id)) {
        this.setState({
          errorType: "employee_id",
          errorText: (
            <span className="text-danger">
              <b>
                Id should be combination of alphanumeric text, underscore and
                hyphen
              </b>
            </span>
          ),
        });
        return;
      }
    }
    // if (name === "") {
    //   this.setState({
    //     errorType: "name",
    //     errorText: (
    //       <span className="text-danger">
    //         <b>Name should not be empty</b>
    //       </span>
    //     ),
    //   });
    //   return;
    // }
    // if (name !== "") {
    //   var filter = /^[a-zA-Z0-9]+([-_\s]{1}[a-zA-Z0-9]+)*$/;

    //   if (!filter.test(name)) {
    //     this.setState({
    //       errorType: "name",
    //       errorText: (
    //         <span className="text-danger">
    //           <b>
    //             {" "}
    //             Name should be combination of alphanumeric text, underscore,
    //             space and hyphen
    //           </b>
    //         </span>
    //       ),
    //     });
    //     return;
    //   }
    // }

    if (salary === "") {
      this.setState({
        errorType: "salary",
        errorText: (
          <span className="text-danger">
            <b>Salary should not be empty</b>
          </span>
        ),
      });
      return;
    }

    if (employeesArray && Object.keys(employeesArray).length > 0) {
      this.updateSalary();
    } else {
      this.addSalary();
    }
  };

  errorShow = (type) => {
    const { errorType, errorText } = this.state;
    return errorType === type ? <p>{errorText}</p> : null;
  };

  handleBack = (e) => {
    e.preventDefault();
    this.props.history.push("/salary");
  };

  resetState = (e) => {
    e.preventDefault();
    const employeesArray =
      (this.props && this.props.history.location.state) || {};
    this.setState({
      employee_id:
        (employeesArray &&
          employeesArray.employee &&
          employeesArray.employee.empId) ||
        "",
      // name:
      //   (employeesArray &&
      //     employeesArray.employee &&
      //     employeesArray.employee.name) ||
      //   "",

      salary: "",
      errorText: "",
      errorType: "",
    });
  };

  render() {
    const { employee_id, loading, salary } = this.state;
    const employeesArray =
      (this.props && this.props.history.location.state) || {};

    return (
      <div>
        <DefaultLayout>
          <Form>
            <Card>
              <CardHeader>
                <Button
                  disabled={loading}
                  onClick={this.handleBack}
                  className="backBtn"
                >
                  <i className="fas fa-arrow-left"></i>Back
                </Button>
              </CardHeader>
              <CardBody className={`f-w-loader ${loading ? "loader" : ""}`}>
                {loading && <Loader />}
                <div className="loader-content-wrapper">
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="employee id">
                        <b>Employee Id</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="text"
                        onChange={this.inputHandler}
                        name="employee_id"
                        value={employee_id}
                        disabled={
                          employeesArray &&
                          employeesArray.employee &&
                          employeesArray.employee.empId
                        }
                        placeholder="Enter employee id"
                      />
                      {this.errorShow("employee_id")}
                    </Col>
                  </FormGroup>
                  {/* <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="name">
                        <b>Name</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="text"
                        placeholder="Enter name"
                        onChange={this.inputHandler}
                        name="name"
                        value={name}
                        disabled={
                          employeesArray &&
                          employeesArray.employee &&
                          employeesArray.employee.empId
                        }
                      />
                      {this.errorShow("name")}
                    </Col>
                  </FormGroup> */}
                  {/* <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="Email">
                        <b>Email</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="email"
                        placeholder="Enter email"
                        onChange={this.inputHandler}
                        name="email"
                        value={email}
                      />
                      {this.errorShow("email")}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="contact">
                        <b>Contact</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="text"
                        // pattern="[789][0-9]{9}"
                        placeholder="Enter your contact number"
                        onChange={this.inputHandler}
                        name="contact"
                        value={contact}
                      />
                      {this.errorShow("contact")}
                    </Col>
                  </FormGroup> */}

                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="contact">
                        <b>Salary</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="text"
                        // pattern="[789][0-9]{9}"
                        placeholder="Enter salary"
                        onChange={this.inputHandler}
                        name="salary"
                        value={salary}
                      />
                      {this.errorShow("salary")}
                    </Col>
                  </FormGroup>

                  <div className="ml-auto mr-0 w-fit-content text-right">
                    <Button
                      type="button"
                      color="success"
                      onClick={this.handleSubmit}
                      disabled={loading}
                    >
                      <i className="fa fa-dot-circle-o" /> Submit
                    </Button>
                    <Button
                      className="ml-3"
                      disabled={loading}
                      type="reset"
                      color="danger"
                      onClick={this.resetState}
                    >
                      <i className="fa fa-ban" /> Reset
                    </Button>
                  </div>
                </div>
              </CardBody>
            </Card>
          </Form>
        </DefaultLayout>
      </div>
    );
  }
}
export default AddSalary;
