/* eslint-disable no-mixed-operators */
import React, { Component, Suspense } from "react";
import { Nav, NavItem, Container } from "reactstrap";
import { Link, withRouter } from "react-router-dom";
import {
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarNav,
} from "@coreui/react";
// import { FilePicker } from "react-file-picker";
// import { NotificationManager } from "react-notifications";
import navigation from "../../_nav";
import { Logout } from "../../assets/image/iconsComponent";

// import Avatar from "../../assets/image/avatar.svg";

const DefaultHeader = React.lazy(() => import("./header"));

class DefaultLayout extends Component {
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );

  componentDidMount() {
    this.addRippleEffect();
  }

  handleClick = (e) => {
    e.preventDefault();
    localStorage.clear();
    localStorage.setItem("isLoggedIn", false);

    this.props.history.push("/");
  };

  addRippleEffect = () => {
    if (document.getElementsByClassName("wave-effect").length > 0)
      for (
        let i = 0;
        i < document.getElementsByClassName("wave-effect").length > 0;
        i++
      ) {
        document.getElementsByClassName("wave-effect")[
          i
        ].onclick = this.renderRipple;
      }
  };
  renderRipple = (e) => {
    if (document.getElementsByClassName("ripple")[0])
      document.getElementsByClassName("ripple")[0].remove();

    let classList = e.currentTarget.classList.contains("nav-item");
    var posX = e.currentTarget.offsetLeft,
      posY = classList
        ? e.currentTarget.offsetTop + 55
        : e.currentTarget.offsetTop,
      buttonWidth = e.currentTarget.offsetWidth,
      buttonHeight = e.currentTarget.offsetHeight;

    if (buttonWidth >= buttonHeight) {
      buttonHeight = buttonWidth;
    } else {
      buttonWidth = buttonHeight;
    }
    var x = e.pageX - posX - buttonWidth / 2;
    var y = e.pageY - posY - buttonHeight / 2;

    var node = document.createElement("span");
    node.setAttribute("class", "ripple");
    node.style.width = buttonWidth + "px";
    node.style.height = buttonHeight + "px";
    node.style.top = y + "px";
    node.style.left = x + "px";

    e.currentTarget.prepend(node);
  };

  render() {
    const user_details = localStorage.getItem("user");
    const today_date = new Date();
    const today_time = today_date.getHours();

    let xyz;
    if (today_time >= 12 && today_time <= 17) {
      xyz = "Good Afternoon";
    }
    if (today_time >= 17 && today_time <= 24) {
      xyz = "Good Evening";
    }
    if (today_time < 12) {
      xyz = "Good Morning";
    }

    return (
      <div className="app">
        <AppHeader fixed>
          <Suspense fallback={this.loading()}>
            <DefaultHeader {...this.props} onLogout={(e) => this.signOut(e)} />
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <Suspense>
              <div className="dayTime">
                {/*<div className="userImg mr-2">
                  <img src={Avatar} alt="" onChange={this.imageUpload} /> */}
                {/* <FilePicker
                    className="button"
                    maxSize={10}
                    buttonText="Upload a file!"
                    extensions={["jpg,jpeg,png"]}
                    // onChange={(file) => this.setState({ file })}
                    // onError={(error) => {
                    //   NotificationManager.warning(
                    //     "Please upload a file of type: txt"
                    //   );
                    // }}
                    // onClear={() => this.setState({ file: {} })}
                    // triggerReset={this.state.reset}
                  >
                    <span className="input-button file-upload" type="button">
                      <div className="d-flex align-items-center ">
                        <img src={Avatar} alt="" onChange={this.imageUpload} />
                      </div>
                    </span>
                  </FilePicker> 
                </div>*/}
                <div>
                  <p>{`${xyz},`}</p>
                  <p>{user_details && JSON.parse(user_details).name}</p>
                </div>
              </div>
              <AppSidebarNav navConfig={navigation} />

              <li className="nav-item logoutBtn d-lg-none">
                <Link to="/" onClick={(e) => this.handleClick(e)}>
                  <Logout />
                  Logout
                </Link>
              </li>
            </Suspense>
            <Nav className="ml-auto" navbar>
              <NavItem className="d-md-down-none" />
            </Nav>
            <AppSidebarFooter />
          </AppSidebar>
          <main className="main pt-4">
            <Container fluid>{this.props.children}</Container>
          </main>
        </div>
      </div>
    );
  }
}

export default withRouter(DefaultLayout);
