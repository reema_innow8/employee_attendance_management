import React, { Component } from "react";
import { apiCall } from "../../common/axios";
import {
  Nav,
  NavItem,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
} from "reactstrap";
import { Link } from "react-router-dom";
import { AppNavbarBrand, AppSidebarToggler } from "@coreui/react";
import logo from "../../assets/image/icons/ams.jpg";
import UserIcon from "../../assets/image/user.jpg";
import { withRouter } from "react-router-dom";
import { LogoutIcon } from "../../assets/image/iconsComponent";

class DefaultHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      notificationsArr: [],
      count: 0,
    };
  }
  // abortController = new AbortController();
  componentDidMount() {
    this.notificationFetch();
    this.notificationCount();
    setInterval(() => {
      this.notificationFetch();
      this.notificationCount();
    }, 500000);
  }
  handleClick = (e) => {
    e.preventDefault();
    localStorage.clear();
    localStorage.setItem("isLoggedIn", false);
    this.props.history.push("/");
  };

  handleNotification = () => {
    this.setState({
      isOpen: !this.state.isOpen,
      count: 0,
    });

    this.notificationRead();
    this.notificationFetch();
    // this.notificationCount();
  };
  notificationRead = () => {
    const notificationsId = this.state.notificationsArr.map(
      ({ _id, index }) => _id
    );

    const notificationObj = {
      id: notificationsId,
      // signal: this.abortController.signal,
    };
    this.setState({
      loading: true,
    });
    apiCall("notification/read", notificationObj).then((res) => {
      const response = res.data;

      if (response.status === 200) {
        this.setState({
          loading: false,
        });
      }
    });
  };
  notificationFetch = () => {
    const isLoggedIn = localStorage && localStorage.getItem("isLoggedIn");
    if (isLoggedIn === "true") {
      const userId = localStorage && localStorage.getItem("userId");
      const notificationObj = {
        id: userId,
        // signal: this.abortController.signal,
      };
      this.setState({
        loading: true,
      });
      apiCall("notification/fetch", notificationObj).then((res) => {
        const response = res.data;
        if (response.status === 200) {
          if (this.state.notificationsArr !== response.data) {
            this.setState({
              loading: false,
              notificationsArr: response && response.data,
            });
          }
        }
      });
    }
  };

  notificationCount = () => {
    const { notificationsArr } = this.state;
    let count = 0;

    for (let ntfn of notificationsArr) {
      if (ntfn.isRead === false) count++;
    }
    this.setState({ count });
  };

  redirectNotification = (data) => {
    const { type } = data;

    if (!data || !type) return;
    // eslint-disable-next-line default-case
    switch (type) {
      case "leave":
        this.props.history.push({
          pathname: "/leaves",
          state: { openLeaveModal: true, data: data.leave },
        });
        break;
      case "user":
        this.props.history.push("/employees");
        break;
    }
  };

  render() {
    const { isOpen, notificationsArr, count } = this.state;
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none toggler" display="md" mobile />
        <AppNavbarBrand
          // href={"/dashboard"}
          full={{ src: logo, alt: "CoreUI Logo", height: 50 }}
        />
        <Nav className="d-lg-block d-none" navbar>
          <NavItem className="px-3" />
        </Nav>
        <Nav className="ml-auto " navbar>
          <div className="bellBtn mr-2" onClick={this.handleNotification}>
            {notificationsArr && notificationsArr.length > 0 && (
              <Dropdown isOpen={isOpen} toggle={this.handleNotification}>
                <DropdownToggle>
                  <div className="bellIcon mr-sm-3">
                    {count > 0 && <span className="count">{count}</span>}
                    <i
                      className={`fas fa-bell ${isOpen ? "text-primary" : ""}`}
                    />
                  </div>
                </DropdownToggle>
                <DropdownMenu>
                  <div className="notification-dropdown">
                    <div className="notificationHeader">
                      <h4 className="mb-0">Notifications</h4>
                      {/* <i className="fas fa-times-circle" /> */}
                    </div>
                    <div className="pb-3">
                      <ul>
                        {notificationsArr &&
                          notificationsArr.length > 0 &&
                          notificationsArr.map((data, index) => {
                            return (
                              <li
                                onClick={() => this.redirectNotification(data)}
                                key={index}
                                className={`${
                                  data.isRead === false ? "current" : ""
                                }`}
                              >
                                <img src={UserIcon} alt="" width="50px" />
                                <p className="mb-0">{data.message}</p>
                              </li>
                            );
                          })}
                      </ul>
                    </div>
                  </div>
                </DropdownMenu>
              </Dropdown>
            )}
          </div>
          <NavItem className="d-sm-block d-none logoutIcon">
            <Link
              to="/"
              className="nav-link"
              onClick={(e) => this.handleClick(e)}
            >
              <LogoutIcon /> Logout
            </Link>
          </NavItem>
          <NavItem />
        </Nav>
      </React.Fragment>
    );
  }
}

export default withRouter(DefaultHeader);
