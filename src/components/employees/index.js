import React, { Component } from "react";
import DefaultLayout from "../../containers/layout/index";
import { Link } from "react-router-dom";
import { FilePicker } from "react-file-picker";
import Loader from "../../loader/index";
import { NotificationManager } from "react-notifications";
import DeleteModal from "../../common/deleteModal";
import EditIcon from "../../assets/image/edit.svg";
import DeleteIcon from "../../assets/image/delete.svg";
import Tooltip from "../tooltip";
import CommonPagination from "../../common/pagination";
import { isAdmin } from "../../common/typeUser";
import moment from "moment";
import * as FileSaver from "file-saver";
import {
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  Button,
  Table,
} from "reactstrap";
import { apiCall } from "../../common/axios";
import { attendancefileFormat } from "../../common/credentials";

class Employees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employees: [],
      // employeesData: [],
      loading: false,
      image: "",
      searchData: "",
      file: {},
      reset: {},
      removeModal: false,
      empId: "",
      tooltipOpen: false,
      activePage: 1,
      offset: 0,
      limit: 10,
      totalLength: 0,
      employeeId: "",
    };
  }

  toggle = () => {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  };

  remove = (e, id, employeeId) => {
    e.stopPropagation();
    this.setState({
      removeModal: !this.state.removeModal,
      empId: id,
      employeeId: employeeId,
    });
  };

  deleteEmployee = (id) => {
    const { employees } = this.state;
    this.setState({
      loading: true,
    });
    const obj = { id };
    apiCall("employee/remove", obj).then((res) => {
      if (res.data.status === 200) {
        NotificationManager.success(res.data.message);
        this.props.history.push("/employees");
        if (employees && employees.length === 1) {
          window.location.reload();
        }
        this.fetch();
      }
      this.setState({
        loading: false,
        removeModal: !this.state.removeModal,
      });
    });
  };

  fetch = (offsetObj) => {
    const { offset, limit } = this.state;
    let stateObj = {
      loading: true,
    };
    const userId = JSON.parse(localStorage.getItem("user")).id;
    let obj = {
      id: userId,
      limit,
    };
    if (offsetObj && offsetObj.searchData) {
      obj.search = offsetObj.searchData;
      obj.offset = 0;
      stateObj.offset = 0;
      stateObj.activePage = 1;
    }
    this.setState(stateObj);
    if (offsetObj && offsetObj.offset >= 0) {
      obj.offset = offsetObj.offset;
    } else if (!(stateObj.offset === 0)) {
      obj.offset = offset;
    }
    apiCall("employee/org-employees", obj)
      .then((res) => {
        const response = res.data;

        if (response.status === 200) {
          this.setState({
            loading: false,
            employees:
              (response && response.data && response.data.employees) || [],
            // employeesData:
            //   (response && response.data && response.data.employees) || [],
            totalLength:
              (response && response.data && response.data.count) || 0,
            searchData: obj.search || "",
          });
        } else {
          this.setState({
            loading: false,
            employees: [],
            totalLength: 0,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };

  componentDidMount() {
    this.fetch();
  }

  handleEdit = (e, data) => {
    e.preventDefault();
    e.stopPropagation();

    this.props.history.push({
      pathname: "/employees/edit",
      state: data,
    });
  };

  handleEmployee = (e, data) => {
    e.preventDefault();
    this.props.history.push({
      pathname: "/employees/detail",
      state: data,
    });
  };

  handleUpload = (file) => {
    const userId = JSON.parse(localStorage.getItem("user")).id;
    const formData = new FormData();
    formData.append("attendance", file);
    formData.append("id", userId);
    apiCall("employee/attendance", formData)
      .then((res) => {
        const response = res.data;
        if (response.status === 200) {
          NotificationManager.success("File has been uploaded successfully");
          this.setState({
            loading: false,
          });
          this.props.history.push({
            pathname: "/employees",
          });
        } else {
          NotificationManager.warning(response.message);
          this.setState({
            loading: false,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };

  // filterByNameOrId = (searchText, list) => {
  //   return list.filter((item) => {
  //     return (
  //       item.name.toLowerCase().includes(searchText.toLowerCase()) ||
  //       item.empId.toLowerCase().includes(searchText.toLowerCase())
  //     );
  //   });
  // };

  // saveSearchData = (e) => {
  //   const { employees } = this.state;
  //   this.setState({
  //     employeesData: this.filterByNameOrId(e.target.value, employees)
  //   });
  // };

  handlePageChange = (pageNumber) => {
    const { limit } = this.state;
    let obj = {
      offset: (pageNumber - 1) * limit,
      loading: true,
      activePage: pageNumber,
    };
    this.setState(obj);
    this.fetch(obj);
  };

  downloadFileFormat = () => {
    // const blob = new Blob(attendancefileFormat, {
    //   type: "text/plain;charset=utf-8",
    // });
    // FileSaver.saveAs(blob, "attendance_file_format.txt");

    // const a = document.createElement("a");
    // link.href = URL.createObjectURL(attendancefileFormat) // DOES NOT WORK
    // link.download = 'attendance_file_format.txt'
    // link.click() // Save

    // var json = JSON.stringify(data),
    const blob = new Blob(attendancefileFormat, {
      type: "text/plain;charset=utf-8",
    });
    FileSaver.saveAs(blob, "attendance_file_format.txt");

    // const url = window.URL.createObjectURL(blob);
    // window.location = url;

    // a.href = url;
    // a.download = "attendance_file_format.txt";
    // a.click();
    // window.URL.revokeObjectURL(url);
  };

  render() {
    const {
      // employeesData,
      employees,
      totalLength,
      loading,
      file,
      removeModal,
      // searchData
    } = this.state;
    // const type = localStorage.getItem("type");
    return (
      <div>
        <DefaultLayout>
          <Card>
            {/* {loading && <Loader />} */}
            <CardHeader>
              <div className="d-flex justify-content-between align-items-center">
                <h5>Employee Details</h5>

                <Link
                  to={{
                    pathname: "/employees/add",
                  }}
                  className="add-empBtn"
                >
                  {isAdmin() && (
                    <Button className="ml-3" color="primary" disabled={loading}>
                      <i className="fas fa-user-plus mr-2" />
                      Add Employee
                    </Button>
                  )}
                </Link>
              </div>
            </CardHeader>
            <CardBody>
              <Row>
                <Col className="text-center">
                  {isAdmin() && (
                    <div className="uploadWrapper">
                      <FilePicker
                        className="button"
                        maxSize={10}
                        buttonText="Upload a file!"
                        extensions={["txt"]}
                        onChange={(file) => this.setState({ file })}
                        onError={(error) => {
                          NotificationManager.warning(
                            "Please upload a file of type: txt"
                          );
                        }}
                        onClear={() => this.setState({ file: {} })}
                        triggerReset={this.state.reset}
                      >
                        <span
                          className="input-button file-upload"
                          type="button"
                        >
                          <i className="fas fa-cloud-upload-alt"></i>
                        </span>
                      </FilePicker>
                      <div className="d-flex align-items-center justify-content-center mt-3">
                        <FilePicker
                          disabled={loading}
                          className="button"
                          maxSize={10}
                          buttonText="Upload a file!"
                          extensions={["txt"]}
                          onChange={(file) => this.setState({ file })}
                          onError={(error) => {
                            NotificationManager.warning(
                              "Please upload a file of type: txt"
                            );
                          }}
                          onClear={() => this.setState({ file: {} })}
                          triggerReset={this.state.reset}
                        >
                          <span
                            className="input-button file-upload"
                            type="button"
                          >
                            <div className="d-flex align-items-center ">
                              <h5 className="my-0">Browse to upload files</h5>
                            </div>
                          </span>
                        </FilePicker>
                        <Button
                          disabled={loading}
                          id="format"
                          style={{
                            textDecoration: "underline",
                            boxShadow: "none",
                          }}
                          className=" file-format"
                          onClick={this.downloadFileFormat}
                        >
                          Download Sample File
                        </Button>
                        <Tooltip placement="right" target="format">
                          Click here to download supported attendance file
                          format
                        </Tooltip>
                      </div>
                      <div className="file-details">
                        <h4> {file.name}</h4>
                      </div>
                      <Button
                        disabled={!file.name}
                        className="mb-3"
                        onClick={(e) => this.handleUpload(file)}
                      >
                        <i className="fas fa-file-upload mr-2" />
                        Upload
                      </Button>
                      {/* <Button
                        id="format"
                        className="mb-3 ml-3 file-format"
                        onClick={this.downloadFileFormat}
                      >
                        Download Sample File
                      </Button>
                      <Tooltip placement="right" target="format">
                        Click here to download supported attendance file format
                      </Tooltip> */}
                    </div>
                  )}
                  {isAdmin() && (
                    <div>
                      <input
                        type="search"
                        placeholder="Search by an employee name or an id"
                        required
                        onChange={(e) =>
                          this.fetch({ searchData: e.target.value })
                        }
                        className="search"
                      />
                      {/* <Link
                      to={{
                        pathname: "/employees/add",
                      }}
                      className="add-empBtn"
                    >
                      <Button className="ml-3" color="primary">
                        <i className="fas fa-user-plus mr-2" />
                        Add Employee
                      </Button>
                    </Link> */}
                    </div>
                  )}
                </Col>
              </Row>
            </CardBody>
            <div className={`f-w-loader ${loading ? "loader" : ""}`}>
              <div className="tableWrapper employee">
                {employees && employees.length > 0 && (
                  <Table responsive striped>
                    <thead>
                      <tr>
                        <th className="text-nowrap">Employee Id</th>
                        <th className="text-nowrap">Date Of Joining</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th></th>
                        {/* {type === "admin" && <th></th>} */}
                      </tr>
                    </thead>
                    <tbody>
                      {
                        // employeesData &&
                        //   employeesData.length > 0 &&
                        employees.map((data, index) => {
                          // const name = data.name.toLowerCase();
                          // const id = data.empId.toLowerCase();
                          // const isTrue = searchData
                          //   ? name.includes(searchData) ||
                          //   id.includes(searchData)
                          //   : true;

                          // if (isTrue) {
                          const joining_date = data.joiningDate
                            ? moment(data.joiningDate).format("DD-MM-YYYY")
                            : "-";
                          return (
                            <tr
                              key={index}
                              onClick={(e) => this.handleEmployee(e, data)}
                            >
                              <td
                                style={{ cursor: "pointer" }}
                                className="text-primary"
                              >
                                {data.empId}
                              </td>
                              <td>{joining_date}</td>
                              <td>{data.name}</td>
                              <td>{data.email}</td>
                              <td>{data.contact}</td>
                              <td className="text-right">
                                {isAdmin() && (
                                  <>
                                    <Link
                                      to={{
                                        pathname: "/employees/edit",
                                      }}
                                    >
                                      <Button
                                        id={`edit-${index}`}
                                        className="text-nowrap editBtn"
                                        disabled={loading}
                                        color="success"
                                        size="sm"
                                        onClick={(e) =>
                                          this.handleEdit(e, data)
                                        }
                                      >
                                        <img src={EditIcon} alt="" />
                                      </Button>
                                    </Link>

                                    <Tooltip
                                      placement="left"
                                      target={`edit-${index}`}
                                    >
                                      Edit
                                    </Tooltip>
                                  </>
                                )}
                                {isAdmin() && (
                                  <>
                                    <span className="ml-2">
                                      <Button
                                        id={`remove-${index}`}
                                        className="text-nowrap editBtn"
                                        disabled={loading}
                                        color="danger"
                                        size="sm"
                                        onClick={(e) =>
                                          this.remove(e, data._id, data.empId)
                                        }
                                      >
                                        <img src={DeleteIcon} alt="" />
                                      </Button>
                                    </span>
                                    <Tooltip
                                      placement="right"
                                      target={`remove-${index}`}
                                    >
                                      Delete
                                    </Tooltip>
                                  </>
                                )}
                              </td>
                            </tr>
                          );
                          // }
                        })
                      }
                    </tbody>
                  </Table>
                )}
                {removeModal && (
                  <DeleteModal
                    isOpen={this.state.removeModal}
                    deleteRecord={this.deleteEmployee}
                    loading={this.state.loading}
                    id={this.state.empId}
                    toggle={this.remove}
                    employeeId={this.state.employeeId}
                  />
                )}
                {loading ? (
                  <Loader />
                ) : (
                  employees &&
                  employees.length === 0 && (
                    <div className="no-records">
                      <h5 className="mb-0">
                        <i>No Records Found</i>
                      </h5>
                    </div>
                  )
                )}
              </div>
            </div>
          </Card>
          {totalLength > 10 && (
            <CommonPagination
              activePage={this.state.activePage}
              itemsCountPerPage={10}
              totalItemsCount={totalLength}
              handlePageChange={this.handlePageChange}
            />
          )}
        </DefaultLayout>
      </div>
    );
  }
}
export default Employees;
