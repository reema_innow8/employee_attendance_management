import React from "react";
import { Container } from "reactstrap";
// import DefaultLayout from "../containers/layout";
import Loader from "../loader/index";
import { apiCall } from "../common/axios";
import { NotificationManager } from "react-notifications";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Button,
  Input,
  FormGroup,
  Label,
} from "reactstrap";
const queryString = require("query-string");

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      newpassword: "",
      confirmpassword: "",
      errorType: "",
      errorText: "",
    };
  }

  handleChange = (e) => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
  };

  clearError() {
    this.setState({
      errorType: "",
      errorText: "",
    });
  }

  update = () => {
    const { newpassword, confirmpassword } = this.state;
    const string = queryString.parse(this.props.location.search);
    const token = string.t;

    if (newpassword === "") {
      this.setState({
        errorType: "newpassword",
        errorText: (
          <span className="text-danger">
            <b>Please enter your new password</b>
          </span>
        ),
      });
      return;
    }
    if (confirmpassword === "") {
      this.setState({
        errorType: "confirmpassword",
        errorText: (
          <span className="text-danger">
            <b>Please confirm your password</b>
          </span>
        ),
      });
      return;
    }

    const obj = {
      token,
      password: newpassword,
      confirm: confirmpassword,
    };
    this.setState({
      loading: true,
    });
    apiCall("auth/verify-token", obj)
      .then((res) => {
        const response = res.data;

        if (response.status === 200) {
          localStorage.setItem("user", JSON.stringify(response.data));
          localStorage.setItem("authToken", response.authToken);
          NotificationManager.success(response.message);
          this.props.history.push("/");
        } else {
          NotificationManager.warning(response.message);
        }
        this.setState({
          loading: false,
          newpassword: "",
          confirmpassword: "",
        });
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
            newpassword: "",
            confirmpassword: "",
          });
        }
      });
  };

  render() {
    const {
      loading,
      errorText,
      errorType,
      newpassword,
      confirmpassword,
    } = this.state;

    return (
      <div>
        {/* <DefaultLayout> */}
        <Container>
          <Card className={`mt-5 f-w-loader ${loading ? "loader" : ""}`}>
            {loading && <Loader />}
            <CardHeader>
              <h5>Change password</h5>
            </CardHeader>
            <div className="loader-content-wrapper">
              <CardBody>
                <FormGroup row>
                  <Col md={3} className="text-left">
                    <Label for="New password">
                      <b>New password</b>
                    </Label>
                    <span className="text-danger">*</span>
                  </Col>
                  <Col md={9}>
                    <Input
                      style={{ maxWidth: "initial" }}
                      type="password"
                      name="newpassword"
                      id="New password"
                      placeholder="Enter new password"
                      onChange={this.handleChange}
                      value={newpassword}
                      disabled={loading}
                    />
                    {errorType === "newpassword" && <p>{errorText}</p>}
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md={3} className="text-left">
                    <Label for="Confirm password">
                      <b>Confirm password </b>
                    </Label>
                    <span className="text-danger">*</span>
                  </Col>
                  <Col md={9}>
                    <Input
                      style={{ maxWidth: "initial" }}
                      type="password"
                      name="confirmpassword"
                      id="Confirm password"
                      placeholder="Enter confirm password"
                      onChange={this.handleChange}
                      value={confirmpassword}
                      disabled={loading}
                    />
                    {errorType === "confirmpassword" && <p>{errorText}</p>}
                  </Col>
                </FormGroup>
                <div className="text-right">
                  <Button
                    color="success"
                    onClick={this.update}
                    disabled={loading}
                  >
                    <i className="fas fa-caret-right mr-2" />
                    Update
                  </Button>
                </div>
              </CardBody>
            </div>
          </Card>
        </Container>
        {/* </DefaultLayout> */}
      </div>
    );
  }
}

export default ForgotPassword;
