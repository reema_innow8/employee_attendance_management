import React from "react";
import DefaultLayout from "../containers/layout";
import { NotificationManager } from "react-notifications";
import moment from "moment";
import MultipleDatePicker from "./MultipleDatePicker";
import DeleteModal from "../common/deleteModal";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Input,
  Label,
  FormGroup,
  Table,
  Col,
  Card,
  CardHeader,
} from "reactstrap";
import { apiCall } from "../common/axios";
import Loader from "../loader/index";
import {
  dateDifference,
  getDates,
  checkHoliday,
} from "../common/calculateTime";
import { titleCase } from "../common/stringFunctions";
import CommonPagination from "../common/pagination";
import { isAdmin } from "../common/typeUser";
class Leaves extends React.Component {
  constructor(props) {
    const empIdUser = localStorage.getItem("empId");
    super(props);
    this.state = {
      loading: false,
      modal: false,
      remove_modal: false,
      subject: "",
      errorType: "",
      errorText: "",
      body: "",
      id: "",
      leavesArr: [],
      status: "",
      reason: "",
      empIdUser: empIdUser,
      leave_application: "",
      type_of_leave: "",
      date: [],
      employeeLeaveDetails: {},
      activePage: 1,
      offset: 0,
      limit: 10,
      totalLength: 0,
      employeeName: "",
      empId: "",
      removeModal: false,
      statusLeave: "",
      search: "",
      searchData: "",
    };
  }
  // filterRecords = () => {
  //   const { leavesArr } = this.state;
  //   const search = this.state.search.trim().replace(/ +/g, " ");
  //   if (!search) return leavesArr;
  //   return (
  //     leavesArr &&
  //     leavesArr.filter((data) => {
  //       let isTrue =
  //         data.employee.name.toLowerCase().includes(search) ||
  //         data.employee.empId.toLowerCase().includes(search);
  //       return isTrue;
  //     })
  //   );
  // };
  static getDerivedStateFromProps(props, state) {
    const { location } = props;
    if (location && location.state && location.state.openLeaveModal === true) {
      const { data } = location.state;
      const obj = {
        remove_modal: !state.remove_modal,
        reason: "",
        id: data && data._id,
        errorText: "",
        employeeLeaveDetails: data,
        leave_application: "",
      };
      props.history.push({
        state: "",
      });
      return {
        ...state,
        ...obj,
      };
    }
    return null;
  }
  // abortController = new AbortController();
  componentDidMount() {
    this.fetch();
  }
  // componentWillUnmount() {
  //   this.abortController.abort();
  // }
  fetch = (offsetObj) => {
    const { offset, limit, searchData } = this.state;

    let stateObj = {
      loading: true,
    };
    const userId = JSON.parse(localStorage.getItem("user")).id;
    let obj = {
      id: userId,
      limit,
    };
    if (offsetObj) {
      if (offsetObj.searchData) {
        obj.search = offsetObj.searchData;
        obj.offset = 0;
        stateObj.offset = 0;
        stateObj.activePage = 1;
      } else if (offsetObj.searchData !== '') {
        obj.search = searchData;
      }
    }

    this.setState(stateObj);
    if (offsetObj && offsetObj.offset >= 0) {
      obj.offset = offsetObj.offset;
    } else if (!(stateObj.offset === 0)) {
      obj.offset = offset;
    }

    apiCall("leave/fetch", obj)
      .then((res) => {
        const response = res.data;
        if (response.status === 200) {
          this.setState({
            loading: false,
            leavesArr:
              (response && response.data && response.data.leaves) || [],
            totalLength:
              (response && response.data && response.data.count) || [],
            searchData: obj.search || "",
          });
          this.props.history.push({
            pathname: "/leaves",
          });
        } else {
          this.setState({
            loading: false,
            leavesArr: [],
            totalLength: 0,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };
  applyLeave = () => {
    this.clearError();
    this.setState({
      id: "",
      subject: "",
      body: "",
      date: [],
      type_of_leave: "",
      modal: !this.state.modal,
      startDate: new Date(),
      endDate: new Date(),
    });
  };
  acceptRejectLeave = () => {
    const { id, reason, leave_application } = this.state;
    if (leave_application === "") {
      this.setState({
        errorType: "leave_application",
        errorText: (
          <span className="text-danger">
            <b>
              Please select whether you approve, reject or cancel the
              application
            </b>
          </span>
        ),
      });
      return false;
    }
    const userId = JSON.parse(localStorage.getItem("user")).id;
    const Obj = {
      id,
      status: leave_application,
      reason,
      userId,
    };
    apiCall("leave/reply", Obj)
      .then((res) => {
        const response = res.data;
        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.setState({
            loading: false,
            remove_modal: false,
            reason: "",
          });
          this.fetch();
        } else {
          NotificationManager.warning(response.message);
          this.setState({
            loading: false,
            remove_modal: false,
            reason: "",
          });
          this.fetch();
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
            remove_modal: false,
            reason: "",
          });
          this.fetch();
        }
      });
  };
  handleApproveRemove = (e, data) => {
    this.setState({
      remove_modal: !this.state.remove_modal,
      reason: "",
      // status: type === "approve" ? "approved" : "rejected",
      id: data && data._id,
      errorText: "",
      employeeLeaveDetails: data,
      leave_application: "",
    });
  };
  handleInput = (e) => {
    e.preventDefault();
    this.clearError();
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  onSubmit = (dates) => {
    this.clearError();
    let arr = this.state.date;
    arr.push({
      dates,
    });
    this.setState({
      date: arr,
    });
  };
  handleApply = () => {
    // const typeUser = localStorage.getItem("type");
    const { id, empIdUser, subject, body, type_of_leave, date } = this.state;
    if (type_of_leave === "") {
      this.setState({
        errorType: "type_of_leave",
        errorText: (
          <span className="text-danger">
            <b>Please select the type of leave</b>
          </span>
        ),
      });
      return false;
    }
    if (date && date.length === 0) {
      this.setState({
        errorType: "date",
        errorText: (
          <span className="text-danger">
            <b>Please select the date</b>
          </span>
        ),
      });
      return false;
    }
    if (id === "" && isAdmin()) {
      this.setState({
        errorType: "id",
        errorText: (
          <span className="text-danger">
            <b>Please enter your id</b>
          </span>
        ),
      });
      return false;
    }
    if (subject === "") {
      this.setState({
        errorType: "subject",
        errorText: (
          <span className="text-danger">
            <b>Please write the reason to apply leave</b>
          </span>
        ),
      });
      return false;
    }
    if (body === "") {
      this.setState({
        errorType: "body",
        errorText: (
          <span className="text-danger">
            <b>Please explain the reason to apply leave</b>
          </span>
        ),
      });
      return false;
    }
    const datesLen = date.length;
    const userId = JSON.parse(localStorage.getItem("user")).id;
    const Obj = {
      type: type_of_leave,
      dates: (date && datesLen && date[datesLen - 1].dates) || [],
      id: isAdmin() ? id : empIdUser,
      subject,
      message: body,
      userId,
    };
    this.setState({
      loading: true,
    });
    apiCall("leave/apply", Obj)
      .then((res) => {
        const response = res.data;
        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.setState({
            loading: false,
            modal: false,
            id: "",
            subject: "",
            body: "",
            date: [],
            type_of_leave: "",
            startDate: new Date(),
            endDate: new Date(),
          });
          this.fetch();
        } else {
          this.setState({
            errorType: "id",
            errorText: (
              <span className="text-danger">
                <b>Incorrect id</b>
              </span>
            ),
            loading: false,
            id: "",
          });
          this.fetch();
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
            modal: false,
            id: "",
            subject: "",
            body: "",
            date: [],
            type_of_leave: "",
            startDate: new Date(),
            endDate: new Date(),
          });
          this.fetch();
        }
      });
  };
  clearError = () => {
    this.setState({
      errorType: "",
      errorText: "",
    });
  };
  errorShow = (type) => {
    const { errorType, errorText } = this.state;
    return errorType === type ? <p>{errorText}</p> : null;
  };
  handleDate = (type, date) => {
    if (type === "startDate") {
      this.setState({
        startDate: date,
      });
    } else {
      this.setState({
        endDate: date,
      });
    }
  };
  addHolidays = (dates) => {
    dates.sort((a, b) => new Date(a) - new Date(b));
    const arr = [...dates];
    const endDate = new Date(dates[dates.length - 1]);
    for (let i = 0; new Date(dates[i]) <= endDate; i++) {
      const days = dateDifference(dates[i], dates[i + 1]).days;
      if (days > 1) {
        const startDate = moment(dates[i]).add(1, "days");
        const endDate = moment(dates[i + 1]).subtract(1, "days");
        const dateArr = getDates(startDate, endDate);
        const cond =
          dateArr &&
          dateArr.every((i) => {
            const day = moment(i).format("ddd");
            const date = moment(i).format("DD MMM");
            if (checkHoliday(day, date)) return true;
            return null;
          });
        if (cond === true) arr.push(...dateArr);
      }
    }
    arr.sort((a, b) => new Date(a) - new Date(b));
    return arr;
  };
  leaveDates = (dates) => {
    let str = "";
    if (dates && dates.length > 0) {
      dates = this.addHolidays(dates);
      for (let i = 0; i < dates.length; i++) {
        let prev = i;
        let j = i;
        let k = 0;
        for (
          ;
          dates[j] &&
          dates[j + 1] &&
          dateDifference(dates[j], dates[j + 1]).days === 1;
          k++, j++
        ) {}
        if (k > 0) {
          str += moment(dates[prev]).format("DD/MM/YYYY");
          if (k > 1) str += " to ";
          else str += ", ";
          i += k;
        }
        str += moment(dates[i]).format("DD/MM/YYYY");
        if (i < dates.length - 1) str += ", ";
      }
    }
    return str;
  };
  handlePageChange = (pageNumber) => {
    const { limit } = this.state;
    let obj = {
      offset: (pageNumber - 1) * limit,
      loading: true,
      activePage: pageNumber,
    };
    this.setState(obj);
    this.fetch(obj);
  };
  remove = (e, id, name, status) => {
    this.setState({
      removeModal: !this.state.removeModal,
      empId: id,
      employeeName: name,
      statusLeave: status,
    });
  };
  cancelLeave = (id) => {
    const { statusLeave } = this.state;
    this.setState({
      loading: true,
    });
    if (statusLeave === "sent" || (isAdmin() && statusLeave === "approved")) {
      this.leaveCancel(id);
    } else if (statusLeave === "approved") {
      this.requestCancellation(id);
    }
  };
  requestCancellation = (id) => {
    const Obj = {
      id,
      status: "approved",
    };
    apiCall("leave/cancellation-request", Obj)
      .then((res) => {
        const response = res.data;
        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.setState({
            loading: false,
            removeModal: false,
          });
          this.fetch();
        } else {
          NotificationManager.warning(response.message);
          this.setState({
            loading: false,
            removeModal: false,
          });
          this.fetch();
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
            removeModal: false,
          });
          this.fetch();
        }
      });
  };
  leaveCancel = (id) => {
    const userId = JSON.parse(localStorage.getItem("user")).id;
    const Obj = {
      id,
      status: "cancelled",
      userId,
    };
    apiCall("leave/reply", Obj)
      .then((res) => {
        const response = res.data;
        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.setState({
            loading: false,
            removeModal: false,
          });
          this.fetch();
        } else {
          NotificationManager.warning(response.message);
          this.setState({
            loading: false,
            removeModal: false,
          });
          this.fetch();
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
            removeModal: false,
          });
          this.fetch();
        }
      });
  };
  render() {
    // const typeUser = localStorage.getItem("type");
    // const searchRecords = this.filterRecords() || [];
    const empIdUser = localStorage.getItem("empId");
    const {
      loading,
      modal,
      subject,
      body,
      id,
      leavesArr,
      totalLength,
      reason,
      remove_modal,
      leave_application,
      employeeLeaveDetails,
      type_of_leave,
      removeModal,
    } = this.state;

    // const { location } = this.props;
    // if (location && location.state && location.state.openLeaveModal === true) {
    //   const { data } = location.state;
    //   const obj = {
    //     remove_modal: true,
    //     reason: "",
    //     id: data && data._id,
    //     errorText: "",
    //     employeeLeaveDetails: data,
    //     leave_application: "",
    //   }
    //   this.state = {
    //     ...this.state,
    //     ...obj
    //   }
    // }
    let leaveType;
    if (employeeLeaveDetails && employeeLeaveDetails.type === "full_day") {
      leaveType = "Full Day";
    } else if (
      employeeLeaveDetails &&
      employeeLeaveDetails.type === "half_day"
    ) {
      leaveType = "Half Day";
    } else {
      leaveType = "Short Leave";
    }
    return (
      <div>
        <DefaultLayout>
          <Card>
            <CardHeader>
              <div className="d-flex justify-content-between align-items-center">
                <h4>Leave Applications</h4>
                {isAdmin() && (
                  <div className="text-center searchWrapper">
                    <input
                      type="search"
                      placeholder="Search by an employee name or an id"
                      required
                      onChange={(e) =>
                        this.fetch({ searchData: e.target.value })
                      }
                      className="search mb-0"
                    />
                  </div>
                )}
                <Button
                  color="success"
                  onClick={this.applyLeave}
                  className="applyBtn"
                  disabled={loading}
                >
                  <i className="fas fa-hand-sparkles mr-2" />
                  Apply Leave
                </Button>
              </div>
            </CardHeader>
            <div className={`f-w-loader ${loading ? "loader" : ""}`}>
              <div className="tableWrapper leave">
                {/* {isAdmin() && (
                  <div className="text-center mt-2">
                    <input
                      type="search"
                      placeholder="Search by an employee name or an id"
                      required
                      onChange={(e) =>
                        this.setState({ search: e.target.value })
                      }
                      className="search"
                    />
                  </div>
                )} */}
                {leavesArr && leavesArr.length > 0 && (
                  <Table responsive striped>
                    <thead>
                      <tr>
                        <th className="text-nowrap">Date</th>
                        <th className="text-nowrap">Emp ID</th>
                        <th className="text-nowrap">Name</th>
                        <th className="text-nowrap">Type of leave</th>
                        <th className="text-nowrap">Subject</th>
                        <th className="text-nowrap">Message</th>
                        <th className="text-nowrap text-center">Status</th>
                        <th></th>
                        {!isAdmin() && (
                          <>
                            <th className="text-nowrap text-center">Notes</th>
                          </>
                        )}
                      </tr>
                    </thead>
                    <tbody>
                      {leavesArr &&
                        leavesArr.length > 0 &&
                        leavesArr.map((data, index) => {
                          const {
                            employee,
                            subject,
                            message,
                            status,
                            reason,
                            type,
                            dates,
                          } = data;
                          let leaveType;
                          if (type === "full_day") {
                            leaveType = "Full Day";
                          } else if (type === "half_day") {
                            leaveType = "Half Day";
                          } else {
                            leaveType = "Short Leave";
                          }
                          return (
                            <tr key={index}>
                              <td className="text-nowrap">
                                {this.leaveDates(dates)}
                              </td>
                              <td className="text-nowrap">
                                {employee && employee.empId}
                              </td>
                              <td className="text-nowrap">
                                {employee && employee.name}
                              </td>
                              <td className="text-nowrap">{leaveType}</td>
                              <td className="text-nowrap">{subject}</td>
                              <td>
                                <p title={message} className="mb-0">
                                  {message}
                                </p>
                              </td>
                              {!isAdmin() && (
                                <>
                                  <td className="text-nowrap">
                                    <p
                                      className={`mb-0 text-center statusText ${
                                        data.status
                                      } ${
                                        data.status === "approved" &&
                                        data.isCancel === true
                                          ? "cancelled"
                                          : ""
                                      }`}
                                    >
                                      {/* {titleCase(status)} */}
                                      {data.status === "approved" &&
                                      data.isCancel === true
                                        ? "Cancellation Requested"
                                        : titleCase(status)}
                                    </p>
                                  </td>
                                  <td>
                                    {status === "sent" && (
                                      <Button
                                        style={{ fontWeight: "600" }}
                                        color="success"
                                        onClick={(e) =>
                                          this.remove(
                                            e,
                                            data._id,
                                            data.employee.name,
                                            data.status
                                          )
                                        }
                                        // className={`statusBtn ${data.status}`}
                                        className="cancelRequest"
                                        disabled={loading}
                                      >
                                        Cancel Leave
                                      </Button>
                                    )}
                                    {status === "approved" &&
                                      data.isCancel === false && (
                                        <Button
                                          style={{ fontWeight: "600" }}
                                          color="success"
                                          onClick={(e) =>
                                            this.remove(
                                              e,
                                              data._id,
                                              data.employee.name,
                                              data.status
                                            )
                                          }
                                          // className={`statusBtn ${data.status}`}
                                          className="cancelRequest"
                                          disabled={loading}
                                        >
                                          Request Cancellation
                                        </Button>
                                      )}
                                  </td>
                                  <td className="text-nowrap text-center">
                                    {reason ? reason : "-"}
                                  </td>
                                </>
                              )}
                              {/* <td
                                className={`${
                                  data.status === "approved"
                                    ? "text-success"
                                    : "text-danger"
                                  }`}
                              >
                                {data.status === "approved" ||
                                data.status === "rejected" ? ( */}
                              {/* <span
                                  style={{
                                    fontWeight: 800,
                                    textTransform: "uppercase",
                                  }}
                                >
                                  {status}
                                </span> 
                              </td>*/}

                              {/* <td>{reason}</td> */}

                              {/* ) : ( */}
                              {isAdmin() && (
                                <>
                                  <td className="text-nowrap text-center">
                                    <Button
                                      style={{ fontWeight: "600" }}
                                      color="success"
                                      disabled={loading}
                                      // disabled={
                                      //   data.status === "approved" ||
                                      //   data.status === "rejected"
                                      // }
                                      onClick={(e) => this.handleEdit(e, data)}
                                      // eslint-disable-next-line react/jsx-no-duplicate-props
                                      onClick={(e) =>
                                        this.handleApproveRemove(e, data)
                                      }
                                      className={`statusBtn ${data.status}`}
                                    >
                                      {/* {data.status === "approved" && (
                                      <i className="fas fa-thumbs-up mr-2" />
                                    )} */}
                                      {status === "sent"
                                        ? "Action"
                                        : titleCase(status)}
                                    </Button>
                                  </td>
                                  <td>
                                    {data.status === "approved" &&
                                      data.isCancel === true && (
                                        <Button
                                          style={{ fontWeight: "600" }}
                                          color="success"
                                          onClick={(e) =>
                                            this.remove(
                                              e,
                                              data._id,
                                              data.employee.name,
                                              data.status
                                            )
                                          }
                                          // className={`statusBtn ${data.status}`}
                                          className="cancelRequest"
                                          disabled={loading}
                                        >
                                          Cancel Leave
                                        </Button>
                                      )}
                                  </td>
                                </>
                              )}
                            </tr>
                          );
                        })}
                    </tbody>
                  </Table>
                )}
                {removeModal && (
                  <DeleteModal
                    isOpen={this.state.removeModal}
                    deleteRecord={this.cancelLeave}
                    loading={this.state.loading}
                    id={this.state.empId}
                    toggle={this.remove}
                    employeeName={this.state.employeeName}
                    statusLeave={this.state.statusLeave}
                  />
                )}
                {loading ? (
                  <Loader />
                ) : (
                  leavesArr &&
                  leavesArr.length === 0 && (
                    <div className="no-records">
                      <h5 className="mb-0">
                        <i>No Records Found</i>
                      </h5>
                    </div>
                  )
                )}
              </div>
            </div>
            <Modal
              isOpen={modal}
              toggle={this.applyLeave}
              className="leaveModal modal-dialog-centered"
            >
              <ModalHeader toggle={this.applyLeave}>Apply Leave</ModalHeader>
              <ModalBody className={`f-w-loader ${loading ? "loader" : ""}`}>
                {loading && <Loader />}
                <div>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="type_of_leave">
                        <b>Type of leave</b>
                      </Label>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="select"
                        onChange={this.handleInput}
                        name="type_of_leave"
                        id="type_of_leave"
                        value={type_of_leave}
                        disabled={loading}
                      >
                        <option value="select">Select</option>
                        <option value="full_day">Full Day</option>
                        <option value="half_day">Half Day</option>
                        <option value="short_leave">Short Leave</option>
                      </Input>
                      {this.errorShow("type_of_leave")}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="date" className="mr-2 mb-0">
                        <b>Date</b>
                      </Label>
                    </Col>
                    <Col md={9} className="date">
                      <MultipleDatePicker
                        style={{ maxWidth: "initial" }}
                        onSubmit={this.onSubmit}
                        name="date"
                        id="date"
                        // minDate={new Date()}
                        disabled={loading}
                      />
                      {this.errorShow("date")}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="id">
                        <b>Employee Id</b>
                      </Label>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        onChange={this.handleInput}
                        type="text"
                        name="id"
                        id="id"
                        value={isAdmin() ? id : empIdUser}
                        disabled={!isAdmin() || loading}
                      />
                      {this.errorShow("id")}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="subject">
                        <b>Subject:</b>
                      </Label>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        onChange={this.handleInput}
                        type="text"
                        name="subject"
                        id="subject"
                        value={subject}
                        disabled={loading}
                      />
                      {this.errorShow("subject")}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="body">
                        <b>Message:</b>
                      </Label>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial", minHeight: "120px" }}
                        onChange={this.handleInput}
                        type="textarea"
                        name="body"
                        id="body"
                        value={body}
                        disabled={loading}
                      />
                      {this.errorShow("body")}
                    </Col>
                  </FormGroup>

                  <div className="text-center apply-leaveBtn mt-4">
                    {/* <Button
                    color="secondary"
                    onClick={this.applyLeave}
                    className="cancelBtn"
                  >
                    Cancel
                  </Button>{" "} */}
                    <Button
                      color="primary"
                      onClick={this.handleApply}
                      disabled={loading}
                    >
                      Apply
                    </Button>
                  </div>
                </div>
              </ModalBody>
            </Modal>
          </Card>
          <Modal
            isOpen={remove_modal}
            toggle={this.handleApproveRemove}
            className="leaveModal mt-5"
          >
            <ModalHeader toggle={this.handleApproveRemove}>
              Employee Detail
            </ModalHeader>
            <ModalBody className={`f-w-loader ${loading ? "loader" : ""}`}>
              {loading && <Loader />}
              <div>
                <FormGroup row>
                  <Col md={3}>
                    <Label for="emp id">Emp Id:</Label>
                  </Col>

                  <Col md={9}>
                    {employeeLeaveDetails &&
                      employeeLeaveDetails.employee &&
                      employeeLeaveDetails.employee.empId}
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md={3}>
                    <Label for="name">Name:</Label>
                  </Col>

                  <Col md={9}>
                    {employeeLeaveDetails &&
                      employeeLeaveDetails.employee &&
                      employeeLeaveDetails.employee.name}
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md={3}>
                    <Label for="type_of_leave">Type of leave:</Label>
                  </Col>

                  <Col md={9}>{leaveType}</Col>
                </FormGroup>
                <FormGroup row>
                  <Col md={3}>
                    <Label for="subject">Subject:</Label>
                  </Col>

                  <Col md={9}>
                    {employeeLeaveDetails && employeeLeaveDetails.subject}
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md={3}>
                    <Label for="message">Message:</Label>
                  </Col>

                  <Col md={9}>
                    {employeeLeaveDetails && employeeLeaveDetails.message}
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md={3}>
                    <Label for="leave_application">Status:</Label>
                  </Col>
                  {(employeeLeaveDetails &&
                    employeeLeaveDetails.status === "approved") ||
                  (employeeLeaveDetails &&
                    employeeLeaveDetails.status === "rejected") ||
                  (employeeLeaveDetails &&
                    employeeLeaveDetails.status === "cancelled") ? (
                    <Col md={9}>
                      {employeeLeaveDetails && employeeLeaveDetails.status}
                    </Col>
                  ) : (
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="select"
                        onChange={this.handleInput}
                        name="leave_application"
                        id="leave_application"
                        value={leave_application}
                        disabled={loading}
                      >
                        <option value="select">Select</option>
                        <option value="approved">Approve</option>
                        <option value="rejected">Reject</option>
                        <option value="cancelled">Cancel</option>
                      </Input>
                      {this.errorShow("leave_application")}
                    </Col>
                  )}
                </FormGroup>
                <FormGroup row className="my-4">
                  <Col md={3}>
                    {employeeLeaveDetails &&
                      (employeeLeaveDetails.status === "approved" ||
                        employeeLeaveDetails.status === "cancelled" ||
                        employeeLeaveDetails.status === "rejected" ||
                        employeeLeaveDetails.status === "sent") &&
                      employeeLeaveDetails.reason !== "" && (
                        <Label for="reason" className="mb-0">
                          Notes:
                        </Label>
                      )}
                  </Col>
                  {(employeeLeaveDetails &&
                    employeeLeaveDetails.status === "approved") ||
                  (employeeLeaveDetails &&
                    employeeLeaveDetails.status === "cancelled") ||
                  (employeeLeaveDetails &&
                    employeeLeaveDetails.status === "rejected") ? (
                    <Col md={9}>
                      {employeeLeaveDetails && employeeLeaveDetails.reason}
                    </Col>
                  ) : (
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial", minHeight: "120px" }}
                        onChange={this.handleInput}
                        type="textarea"
                        name="reason"
                        id="reason"
                        value={reason}
                        disabled={loading}
                      ></Input>
                      {this.errorShow("reason")}
                    </Col>
                  )}
                </FormGroup>
                <div className="text-right apply-leaveBtn pt-2">
                  {/* <Button
                  color="secondary"
                  onClick={this.handleApproveRemove}
                  className="cancelBtn"
                >
                  Cancel
                </Button>{" "} */}
                  {employeeLeaveDetails &&
                    employeeLeaveDetails.status === "sent" && (
                      <Button
                        color="primary"
                        // disabled={
                        //   (employeeLeaveDetails &&
                        //     employeeLeaveDetails.status === "approved") ||
                        //   (employeeLeaveDetails &&
                        //     employeeLeaveDetails.status === "rejected") ||
                        //   (employeeLeaveDetails &&
                        //     employeeLeaveDetails.status === "cancelled")
                        // }
                        onClick={this.acceptRejectLeave}
                        disabled={loading}
                      >
                        Submit
                      </Button>
                    )}
                </div>
              </div>
            </ModalBody>
          </Modal>
          {totalLength > 10 && (
            <CommonPagination
              activePage={this.state.activePage}
              itemsCountPerPage={10}
              totalItemsCount={totalLength}
              handlePageChange={this.handlePageChange}
            />
          )}
        </DefaultLayout>
      </div>
    );
  }
}

export default Leaves;
