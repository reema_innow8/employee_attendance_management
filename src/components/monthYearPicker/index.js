import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

function styleInject(css, ref) {
  if (ref === void 0) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === "undefined") {
    return;
  }

  var head = document.head || document.getElementsByTagName("head")[0];
  var style = document.createElement("style");
  style.type = "text/css";

  if (insertAt === "top") {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css =
  "@import url(\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\");\n@import url(\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\");\n@font-face {\n  font-family: 'Open Sans';\n  font-style: italic;\n  font-weight: 300;\n  src: local('Open Sans Light Italic'), local('OpenSans-LightItalic'), url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWyV9hrIqY.ttf) format('truetype');\n}\n@font-face {\n  font-family: 'Open Sans';\n  font-style: italic;\n  font-weight: 400;\n  src: local('Open Sans Italic'), local('OpenSans-Italic'), url(https://fonts.gstatic.com/s/opensans/v15/mem6YaGs126MiZpBA-UFUK0Zdcg.ttf) format('truetype');\n}\n@font-face {\n  font-family: 'Open Sans';\n  font-style: italic;\n  font-weight: 700;\n  src: local('Open Sans Bold Italic'), local('OpenSans-BoldItalic'), url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWiUNhrIqY.ttf) format('truetype');\n}\n@font-face {\n  font-family: 'Open Sans';\n  font-style: normal;\n  font-weight: 300;\n  src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN_r8OUuhs.ttf) format('truetype');\n}\n@font-face {\n  font-family: 'Open Sans';\n  font-style: normal;\n  font-weight: 400;\n  src: local('Open Sans Regular'), local('OpenSans-Regular'), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVZ0e.ttf) format('truetype');\n}\n@font-face {\n  font-family: 'Open Sans';\n  font-style: normal;\n  font-weight: 700;\n  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN7rgOUuhs.ttf) format('truetype');\n}\n.month-year-picker {\n  margin: auto;\n  width: 400px;\n  margin-top: 55px;\n}\n.month-year-picker .caption {\n  padding-left: 10px;\n  margin-bottom: 20px;\n  display: block;\n}\n.month-year-picker .year-picker {\n  position: relative;\n  width: 100%;\n  margin-bottom: 20px;\n}\n.month-year-picker .year-picker span {\n  font-size: 25px;\n  color: #5186ed;\n  text-align: center;\n  width: 100%;\n  display: block;\n}\n.month-year-picker .year-picker .controls {\n  position: absolute;\n  right: 0;\n  width: 80px;\n  top: 0;\n  height: 100%;\n  display: flex;\n}\n.month-year-picker .year-picker .controls .fa {\n  margin: auto;\n  cursor: pointer;\n  font-size: 25px;\n}\n.month-year-picker .year-picker .controls .fa.disabled {\n  cursor: default;\n  opacity: 0.3;\n}\n.month-year-picker .month-picker {\n  position: relative;\n  width: 100%;\n}\n.month-year-picker .month-picker > div {\n  position: relative;\n  width: 100%;\n  margin: auto;\n  display: block;\n}\n.month-year-picker .month-picker > div > div {\n  background-color: #ddd;\n  color: cornflowerblue;\n  height: 55px;\n  margin: 10px;\n  width: 20%;\n  display: inline-flex;\n  cursor: pointer;\n}\n.month-year-picker .month-picker > div > div.selected {\n  background-color: #5186ed;\n  color: #FFFFFF;\n}\n.month-year-picker .month-picker > div > div span {\n  margin: auto;\n  font-size: 15px;\n  font-style: normal;\n}\n";
styleInject(css);

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = (function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
})();

var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError(
      "Super expression must either be null or a function, not " +
        typeof superClass
    );
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true,
    },
  });
  if (superClass)
    Object.setPrototypeOf
      ? Object.setPrototypeOf(subClass, superClass)
      : (subClass.__proto__ = superClass);
};

var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError(
      "this hasn't been initialised - super() hasn't been called"
    );
  }

  return call && (typeof call === "object" || typeof call === "function")
    ? call
    : self;
};

var MonthYearPicker = (function (_PureComponent) {
  inherits(MonthYearPicker, _PureComponent);

  function MonthYearPicker(props) {
    classCallCheck(this, MonthYearPicker);

    var _this = possibleConstructorReturn(
      this,
      (
        MonthYearPicker.__proto__ || Object.getPrototypeOf(MonthYearPicker)
      ).call(this, props)
    );

    _this.handleOnClickLeftArrow = _this.handleOnClickLeftArrow.bind(_this);
    _this.handleOnClickRightArrow = _this.handleOnClickRightArrow.bind(_this);
    return _this;
  }

  createClass(MonthYearPicker, [
    {
      key: "handleOnClickLeftArrow",
      value: function handleOnClickLeftArrow() {
        if (this.props.selectedYear <= this.props.minYear) {
          return;
        }
        this.props.onChangeYear(this.props.selectedYear - 1);
      },
    },
    {
      key: "handleOnClickRightArrow",
      value: function handleOnClickRightArrow() {
        if (this.props.selectedYear >= this.props.maxYear) {
          return;
        }
        this.props.onChangeYear(this.props.selectedYear + 1);
      },
    },
    {
      key: "renderMonth",
      value: function renderMonth(month) {
        var _this2 = this;

        var text = "";
        switch (month) {
          case 1:
            text = "Jan";
            break;
          case 2:
            text = "Feb";
            break;
          case 3:
            text = "Mar";
            break;
          case 4:
            text = "Apr";
            break;
          case 5:
            text = "May";
            break;
          case 6:
            text = "Jun";
            break;
          case 7:
            text = "Jul";
            break;
          case 8:
            text = "Aug";
            break;
          case 9:
            text = "Sep";
            break;
          case 10:
            text = "Oct";
            break;
          case 11:
            text = "Nov";
            break;
          case 12:
            text = "Dec";
            break;
          default:
            break;
        }
        var className = this.props.selectedMonth === month ? "selected" : "";

        const _className =
          this.props.selectedYear === new Date().getFullYear()
            ? month > new Date().getMonth() + 1
              ? "cursor-notAllowed"
              : ""
            : "";

        return React.createElement(
          "div",
          {
            className: cx(className, _className),
            role: "button",
            tabIndex: 0,
            onClick: function onClick() {
              return _this2.props.onChangeMonth(month);
            },
          },
          React.createElement("span", null, text)
        );
      },
    },
    {
      key: "renderLeftArrowButton",
      value: function renderLeftArrowButton() {
        if (this.props.selectedYear === this.props.minYear) {
          return React.createElement("i", {
            className: "fa fa-chevron-left disabled",
          });
        }
        return React.createElement("i", {
          role: "button",
          tabIndex: 0,
          onClick: this.handleOnClickLeftArrow,
          className: "fa fa-chevron-left",
        });
      },
    },
    {
      key: "renderRightArrowButton",
      value: function renderRightArrowButton() {
        if (this.props.selectedYear === this.props.maxYear) {
          return React.createElement("i", {
            className: "fa fa-chevron-right disabled",
          });
        }
        return React.createElement("i", {
          role: "button",
          tabIndex: 0,
          onClick: this.handleOnClickRightArrow,
          className: "fa fa-chevron-right",
        });
      },
    },
    {
      key: "render",
      value: function render() {
        return React.createElement(
          "div",
          { className: "month-year-picker" },
          React.createElement(
            "div",
            { className: "wrapper" },
            React.createElement(
              "span",
              { className: "caption" },
              this.props.caption
            ),
            React.createElement(
              "div",
              { className: "year-picker" },
              React.createElement("span", null, this.props.selectedYear),
              React.createElement(
                "div",
                { className: "controls" },
                this.renderLeftArrowButton(),
                this.renderRightArrowButton()
              )
            )
          ),
          React.createElement(
            "div",
            { className: "month-picker" },
            React.createElement(
              "div",
              null,
              this.renderMonth(1),
              this.renderMonth(2),
              this.renderMonth(3),
              this.renderMonth(4)
            ),
            React.createElement(
              "div",
              null,
              this.renderMonth(5),
              this.renderMonth(6),
              this.renderMonth(7),
              this.renderMonth(8)
            ),
            React.createElement(
              "div",
              null,
              this.renderMonth(9),
              this.renderMonth(10),
              this.renderMonth(11),
              this.renderMonth(12)
            )
          )
        );
      },
    },
  ]);
  return MonthYearPicker;
})(PureComponent);

MonthYearPicker.propTypes = {
  caption: PropTypes.string,
  selectedYear: PropTypes.number.isRequired,
  selectedMonth: PropTypes.number.isRequired,
  minYear: PropTypes.number.isRequired,
  maxYear: PropTypes.number.isRequired,
  onChangeYear: PropTypes.func.isRequired,
  onChangeMonth: PropTypes.func.isRequired,
};

MonthYearPicker.defaultProps = {
  caption: "Select month and year",
};

export default MonthYearPicker;
//# sourceMappingURL=index.es.js.map
