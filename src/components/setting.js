import React, { Component } from "react";
import DefaultLayout from "../containers/layout/index";
import Loader from "../loader/index";
import { NotificationManager } from "react-notifications";
import UpdateIcon from "../assets/image/load.svg";
import { isAdmin } from "../common/typeUser";
import CreditCardInput from "react-credit-card-input";
import styled from "styled-components";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Button,
  Input,
  FormGroup,
  Label,
  Row,
} from "reactstrap";
import { apiCall } from "../common/axios";
// eslint-disable-next-line no-undef
const FormCheck = styled(FormGroup)`
  margin-top: 20px;
  &:first-of-type {
    margin-top: 0;
  }
  label {
    font-size: 11px;
    line-height: 22px;
  }
`;
class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      username: "",
      email: "",
      oldpassword: "",
      newpassword: "",
      confirmpassword: "",
      errorType: "",
      errorText: "",
      premiumAmount: 50,
      creditCard: {
        cardNumber: "",
        expiry: "",
        cvc: "",
      },
    };
  }
  clearError = () => {
    this.setState({
      errorType: "",
      errorText: "",
    });
  };
  errorShow = (type) => {
    const { errorType, errorText } = this.state;
    return errorType === type ? (
      <span className="error">
        <b>{errorText}</b>
      </span>
    ) : null;
  };
  handleChange = (e) => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
    this.clearError();
  };

  update = () => {
    const { oldpassword, newpassword, confirmpassword } = this.state;
    if (oldpassword === "") {
      this.setState({
        errorType: "oldpassword",
        errorText: (
          <span className="text-danger">
            <b>Please enter old password</b>
          </span>
        ),
      });
      return false;
    }

    if (newpassword === "") {
      this.setState({
        errorType: "newpassword",
        errorText: (
          <span className="text-danger">
            <b>Please enter new password</b>
          </span>
        ),
      });
      return false;
    }
    if (confirmpassword === "") {
      this.setState({
        errorType: "confirmpassword",
        errorText: (
          <span className="text-danger">
            <b>Please confirm your password</b>
          </span>
        ),
      });
      return false;
    }

    this.setState({
      loading: true,
    });
    const user_details = localStorage.getItem("user");

    const changePwd = {
      id: JSON.parse(user_details).id,
      old: oldpassword,
      new: newpassword,
      confirm: confirmpassword,
    };
    apiCall("auth/change-password", changePwd)
      .then((res) => {
        if (res.data.status === 200) {
          NotificationManager.success("Password has changed successfully");
          this.setState({
            loading: false,
            oldpassword: "",
            newpassword: "",
            confirmpassword: "",
          });
        } else {
          NotificationManager.warning(res.data.message);
          this.setState({
            loading: false,
            oldpassword: "",
            newpassword: "",
            confirmpassword: "",
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
            oldpassword: "",
            newpassword: "",
            confirmpassword: "",
          });
        }
      });
  };
  goPremium = () => {
    // const { creditCard, premiumAmount, user } = this.state;
    // if (!window.Stripe) {
    //   alert(
    //     `There seems to be an issue with Stripe. Try refreshing this page.`
    //   );
    // } else {
    //   this.setState({ loading: true });
    //   const expiryArr = creditCard.expiry.split("/");
    //   const exp_month = parseInt(expiryArr[0], 10);
    //   const exp_year = parseInt(expiryArr[1], 10);
    //   window.Stripe.card.createToken(
    //     {
    //       number: creditCard.cardNumber,
    //       cvc: creditCard.cvc,
    //       exp_month,
    //       exp_year
    //     },
    //     async (status, res) => {
    //       if (res.error) {
    //         alert(res.error.message);
    //       } else if (status !== 200) {
    //         alert(`Something went wrong!`);
    //       } else {
    //         const body = {
    //           amount: premiumAmount * 100,
    //           currency: "nzd",
    //           source: res.id,
    //           description: "Payment to Yelsa via Admin",
    //           id: user.userId,
    //           type: "buyer"
    //         };
    //         try {
    //           const resp = await genericPayment(body);
    //           await this.fetchUser();
    //         } catch (e) {
    //           alert(e);
    //         }
    //       }
    //       this.setState({ loading: false });
    //     }
    //   );
    // }
  };
  render() {
    // const type = localStorage.getItem("type");
    const {
      loading,
      oldpassword,
      newpassword,
      confirmpassword,
      premiumAmount,
      creditCard,
    } = this.state;
    const user_details = localStorage.getItem("user");

    return (
      <div>
        <DefaultLayout>
          <Card>
            <CardHeader>
              {isAdmin() ? <h5>Admin Details</h5> : <h5>Employee Details</h5>}
            </CardHeader>
            <CardBody>
              <FormGroup row>
                <Col md={3} className="text-left">
                  <Label>
                    <b>Name</b>
                  </Label>
                </Col>
                <Col md={9}>
                  <Label>{user_details && JSON.parse(user_details).name}</Label>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md={3} className="text-left">
                  <Label for="email">
                    <b>Email</b>
                  </Label>
                </Col>
                <Col md={9}>
                  <Label>
                    {user_details && JSON.parse(user_details).email}
                  </Label>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md={3} className="text-left">
                  <Label for="email">
                    <b>Contact</b>
                  </Label>
                </Col>
                <Col md={9}>
                  <Label>
                    {user_details && JSON.parse(user_details).contact}
                  </Label>
                </Col>
              </FormGroup>
            </CardBody>
          </Card>
          <Card>
            <CardHeader>
              <h5>Change password</h5>
            </CardHeader>
            <div className={`f-w-loader ${loading ? "loader" : ""}`}>
              {loading && <Loader />}
              <div className="loader-content-wrapper">
                <CardBody>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="Old password">
                        <b>Old password</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="password"
                        name="oldpassword"
                        id="Old password"
                        placeholder="Enter old password"
                        onChange={this.handleChange}
                        value={oldpassword}
                      />
                      {this.errorShow("oldpassword")}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="New password">
                        <b>New password</b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="password"
                        name="newpassword"
                        id="New password"
                        placeholder="Enter new password"
                        onChange={this.handleChange}
                        value={newpassword}
                      />
                      {this.errorShow("newpassword")}
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md={3} className="text-left">
                      <Label for="Confirm password">
                        <b>Confirm password </b>
                      </Label>
                      <span className="text-danger">*</span>
                    </Col>
                    <Col md={9}>
                      <Input
                        style={{ maxWidth: "initial" }}
                        type="password"
                        name="confirmpassword"
                        id="Confirm password"
                        placeholder="Enter confirm password"
                        onChange={this.handleChange}
                        value={confirmpassword}
                      />
                      {this.errorShow("confirmpassword")}
                    </Col>
                  </FormGroup>
                  <div className="text-right">
                    <Button
                      color="success"
                      onClick={this.update}
                      disabled={loading}
                    >
                      Update
                      <img
                        src={UpdateIcon}
                        alt=""
                        className="ml-2 updateIcon"
                      />
                    </Button>
                  </div>
                </CardBody>
              </div>
            </div>
          </Card>
          {isAdmin() && (
            <Card>
              <CardHeader>
                <h5>Going Premium</h5>
              </CardHeader>
              <CardBody className="p-3 profile-card premium-card">
                <Row>
                  <Col xs="7">
                    <FormGroup tag="fieldset" style={{ height: "100%" }}>
                      <FormCheck check>
                        <Label check>
                          <Input
                            onClick={() => this.setState({ premiumAmount: 50 })}
                            type="radio"
                            name="radio1"
                            defaultChecked={premiumAmount === 50}
                          />{" "}
                          <b>$50</b> Standard
                        </Label>
                      </FormCheck>
                      <FormCheck check>
                        <Label check>
                          <Input
                            onClick={() => this.setState({ premiumAmount: 75 })}
                            type="radio"
                            name="radio1"
                            defaultChecked={premiumAmount === 75}
                          />{" "}
                          <b>$75</b> Premium
                        </Label>
                      </FormCheck>
                      <FormCheck check>
                        <Label check>
                          <Input
                            onClick={() => this.setState({ premiumAmount: 99 })}
                            type="radio"
                            name="radio1"
                            defaultChecked={premiumAmount === 99}
                          />{" "}
                          <b>$99</b> Ultra Premium
                        </Label>
                      </FormCheck>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <CreditCardInput
                        fieldStyle={{
                          border: "1px solid #E4E7EA",
                        }}
                        cardNumberInputProps={{
                          value: creditCard.cardNumber,
                          onChange: (event) =>
                            this.setState({
                              creditCard: {
                                ...creditCard,
                                cardNumber: event.target.value,
                              },
                            }),
                        }}
                        cardExpiryInputProps={{
                          value: creditCard.expiry,
                          onChange: (event) => {
                            this.setState({
                              creditCard: {
                                ...creditCard,
                                expiry: event.target.value,
                              },
                            });
                          },
                        }}
                        cardCVCInputProps={{
                          value: creditCard.cvc,
                          onChange: (event) =>
                            this.setState({
                              creditCard: {
                                ...creditCard,
                                cvc: event.target.value,
                              },
                            }),
                        }}
                        fieldClassName="input"
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Button
                      color="primary"
                      onClick={() => {
                        this.goPremium();
                      }}
                    >
                      Pay Now
                    </Button>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          )}
        </DefaultLayout>
      </div>
    );
  }
}
export default Setting;
