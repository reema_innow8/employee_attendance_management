import React from "react";
import { apiCall } from "../../common/axios";
import DefaultLayout from "../../containers/layout";
// import _ from 'lodash';
import { NotificationManager } from "react-notifications";
import {
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import PublicHolidays from "./publicHolidays";
import { setWeekOffs } from "../../common/changeDataFormat";
import { isAdmin } from "../../common/typeUser";
class Holidays extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      weekDays: {},
      checked: false,
      icon: false,
      errorType: "",
      errorText: "",
      plusIcon: false,
      loading: false,
      type: "add",
    };
  }

  clearError = () => {
    this.setState({
      errorType: "",
      errorText: "",
    });
  };
  componentDidMount() {
    this.setState({
      loading: true,
    });

    apiCall("week-off/fetch")
      .then((res) => {
        const { data, status } = res.data;
        if (status === 200 && data) {
          setWeekOffs(data);
          this.setState({
            weekDays: data,
            loading: false,
          });
        } else {
          this.setState({
            loading: false,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  }
  errorShow = (type) => {
    const { errorType, errorText } = this.state;
    return errorType === type ? (
      <span className="error">
        <b>{errorText}</b>
      </span>
    ) : null;
  };

  editWeekOffs = () => {
    const { weekDays } = this.state;
    this.setState({
      loading: true,
    });
    const obj = {
      ...weekDays,
      id: weekDays._id,
    };
    apiCall("week-off/edit", obj)
      .then((res) => {
        const { status, data } = res.data;
        if (status === 200 && data) {
          setWeekOffs(data);
          this.setState({
            loading: false,
            weekDays: data,
          });
          NotificationManager.success("Week Offs updated successfully.");
        } else {
          this.setState({
            loading: false,
          });
        }
      })
      .catch(() => {
        this.setState({
          loading: false,
        });
      });
  };

  weekOffChange = (type) => {
    const { weekDays } = this.state;
    const obj = {
      ...weekDays,
      [type]: !weekDays[type],
    };
    this.setState({ weekDays: obj });
  };

  render() {
    const { weekDays, loading } = this.state;
    // const type = localStorage.getItem("type");

    const days = [
      "monday",
      "tuesday",
      "wednesday",
      "thursday",
      "friday",
      "saturday",
      "sunday",
    ];
    return (
      <div>
        <DefaultLayout>
          {weekDays && Object.keys(weekDays).length > 0 && (
            <Card>
              {/* {loading && <Loader />} */}
              <CardHeader>
                <div className="d-flex justify-content-between align-items-center">
                  {isAdmin() ? <h5>Select Week Offs</h5> : <h5> Week Offs</h5>}
                  {isAdmin() && (
                    <Button
                      className="ml-3"
                      color="primary"
                      onClick={this.editWeekOffs}
                      disabled={loading}
                    >
                      {/* <i className="fas fa-user-plus mr-2" /> */}
                      Save
                    </Button>
                  )}
                </div>
              </CardHeader>
              <div className="divider"></div>
              <CardBody className={`f-w-loader ${loading ? "loader" : ""}`}>
                <Row>
                  {isAdmin()
                    ? days &&
                      days.length > 0 &&
                      days.map((data, index) => (
                        <Col sm={4} lg={3} key={index}>
                          <FormGroup check>
                            <Label check className="text-capitalize">
                              {weekDays[data] ? (
                                <Input
                                  type="checkbox"
                                  checked
                                  onChange={(e) => this.weekOffChange(data)}
                                />
                              ) : (
                                <Input
                                  type="checkbox"
                                  onChange={(e) => this.weekOffChange(data)}
                                />
                              )}
                              {data}
                            </Label>
                          </FormGroup>
                        </Col>
                      ))
                    : days &&
                      days.length > 0 &&
                      days.map((data, index) => (
                        <Col sm={4} lg={3} key={index}>
                          <FormGroup check>
                            <Label check className="text-capitalize">
                              {weekDays[data] ? (
                                <Input type="checkbox" checked disabled />
                              ) : (
                                <Input type="checkbox" disabled />
                              )}
                              {data}
                            </Label>
                          </FormGroup>
                        </Col>
                      ))}
                </Row>
                {this.errorShow("add_event")}
              </CardBody>
            </Card>
          )}
          <PublicHolidays history={this.props.history} />
        </DefaultLayout>
      </div>
    );
  }
}

export default Holidays;
