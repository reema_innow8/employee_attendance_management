import React from "react";
import { apiCall } from "../../../common/axios";
import DatePicker from "react-datepicker";
import moment from "moment";
import { NotificationManager } from "react-notifications";
import cx from "classnames";
import Loader from "../../../loader/index";
import Tooltip from "../../tooltip";
import DeleteModal from "../../../common/deleteModal";
import CommonPagination from "../../../common/pagination";
import {
  Card,
  CardBody,
  CardHeader,
  Button,
  Table,
  FormGroup,
  Input,
  Collapse,
} from "reactstrap";
import EditIcon from "../../../assets/image/edit.svg";
import DeleteIcon from "../../../assets/image/delete.svg";
import { setPublicHolidays } from "../../../common/changeDataFormat";
import { isAdmin } from "../../../common/typeUser";

export default class PublicHolidays extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editId: "",
      addNewHoliday: false,
      weekDays: {},
      startDate: "",
      checked: false,
      add_event: "",
      holidays: [],
      icon: false,
      errorType: "",
      errorText: "",
      plusIcon: false,
      loading: false,
      type: "add",
      id: "",
      removeModal: false,
      activePage: 1,
      offset: 0,
      limit: 10,
      totalLength: 0,
      eventName: "",
    };
  }

  clearError = () => {
    this.setState({
      errorType: "",
      errorText: "",
    });
  };
  componentDidMount() {
    this.setState({
      loading: true,
    });
    this.fetch();
  }
  errorShow = (type) => {
    const { errorType, errorText } = this.state;
    return errorType === type ? (
      <span className="error">
        <b>{errorText}</b>
      </span>
    ) : null;
  };
  handleStartDate = (date) => {
    this.clearError();
    date = new Date(date);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    this.setState({
      startDate: date,
    });
    this.clearError();
  };
  handleAddNewHoliday = () => {
    this.setState({
      addNewHoliday: true,
      editId: "",
      add_event: "",
      startDate: "",
      plusIcon: true,
    });
  };

  handleInput = (e) => {
    this.clearError();
    this.setState({ [e.target.name]: e.target.value });
    this.clearError();
  };
  handleIcon = () => {
    this.clearError();
    const { add_event, startDate, type } = this.state;
    this.setState({
      icon: true,
      editId: "",
      addNewHoliday: false,
    });
    if (startDate === "") {
      this.setState({
        errorType: "startDate",
        errorText: (
          <span className="text-danger">
            <b>Add date of an event</b>
          </span>
        ),
      });
      return false;
    }

    if (add_event === "") {
      this.setState({
        errorType: "add_event",
        errorText: (
          <span className="text-danger">
            <b>Add an event name</b>
          </span>
        ),
      });
      return false;
    } else {
      if (type === "add") {
        this.addHolidays();
      } else {
        this.updateHolidays();
      }
    }
  };
  addHolidays = () => {
    const { add_event, startDate } = this.state;

    this.setState({
      loading: true,
    });
    const holidayObj = {
      title: add_event,
      date: startDate,
    };

    apiCall("holiday/add", holidayObj)
      .then((res) => {
        const response = res.data;

        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.setState({
            holidays: response && response.data,
            loading: false,
            plusIcon: false,
            addNewHoliday: false,
            startDate: "",
            add_event: "",
          });
          this.fetch();
        } else {
          NotificationManager.warning(response.message);
          this.setState({
            loading: false,
            plusIcon: false,
            addNewHoliday: false,
            startDate: "",
            add_event: "",
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };
  updateHolidays = () => {
    const { add_event, startDate, id } = this.state;
    this.setState({
      loading: true,
    });
    const holidayObj = {
      title: add_event,
      date: startDate,
      id,
    };

    apiCall("holiday/edit", holidayObj)
      .then((res) => {
        const response = res.data;

        if (response.status === 200) {
          NotificationManager.success(response.message);
          this.setState({
            loading: false,
            type: "add",
            add_event: "",
            startDate: "",
          });
          this.fetch();
        } else {
          NotificationManager.warning(response.message);
          this.setState({
            loading: false,
            type: "add",
            add_event: "",
            startDate: "",
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
            type: "add",
          });
        }
      });
  };

  handleEdit = (e, data) => {
    const date_formatted = new Date(data.date);
    this.setState({
      errorType: "",
      errorText: "",
      type: true,
      plusIcon: true,
      addNewHoliday: false,
      startDate: date_formatted,
      add_event: data.title,
      id: data._id,
      editId: data._id,
    });
  };
  cancleEdit = () => {
    this.setState({ editId: "" });
  };

  cancleAddNewHoliday = () => {
    this.clearError();
    this.setState({ addNewHoliday: false });
  };

  remove = (e, data) => {
    this.setState({
      id: data && data._id,
      editId: "",
      addNewHoliday: false,
      removeModal: !this.state.removeModal,
      eventName: data && data.title,
    });
  };
  deleteHoliday = (id) => {
    const { holidays } = this.state;

    this.setState({
      loading: true,
    });
    const obj = {
      id,
    };
    apiCall("holiday/remove", obj).then((res) => {
      if (res.data.status === 200) {
        NotificationManager.success(res.data.message);
        this.props.history.push("/holidays");
        if (holidays && holidays.length === 1) {
          window.location.reload();
        }
        this.fetch();
        this.setState({
          loading: false,
          removeModal: !this.state.removeModal,
          startDate: "",
          add_event: "",
        });
      }
    });
  };
  fetch = (offsetObj) => {
    const { offset, limit } = this.state;
    this.setState({
      loading: true,
    });
    let obj = {
      limit,
    };
    if (offsetObj) {
      obj.offset = offsetObj.offset;
    } else {
      obj.offset = offset;
    }

    apiCall("holiday/fetch", obj)
      .then((res) => {
        const { data, status } = res.data;

        if (status === 200) {
          const curDate = new Date();
          const curYear = curDate.getFullYear();
          const curMonth = curDate.getMonth();
          const startDate = new Date(curYear, curMonth, 1);

          let holidays;
          holidays = data.holidays.filter((i) => {
            const cond = new Date(i.date) >= startDate;
            if (cond) return i;
            return null;
          });
          setPublicHolidays(holidays);
          this.setState({
            loading: false,
            holidays: data.holidays || [],
            totalLength: (data && data.count) || [],
          });
        } else {
          this.setState({
            loading: false,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };
  datepicker = () => (
    <div className="datePikkerIconWrapper mr-sm-2">
      <DatePicker
        selected={this.state.startDate}
        onChange={this.handleStartDate}
        dateFormat="dd/MM/yyyy"
        placeholderText="Select Date"
      />
      <i className="far fa-calendar-alt text-primary" />

      {this.errorShow("startDate")}
    </div>
  );
  eventInput = () => (
    <FormGroup className="m-0 left-icon">
      <Input
        onChange={this.handleInput}
        type="text"
        name="add_event"
        id="add_event"
        value={this.state.add_event}
        placeholder="Add event"
      />
      <i className="far fa-calendar-check text-primary" />
      {this.errorShow("add_event")}
    </FormGroup>
  );

  actionButtons = (editId, data, index) => {
    return (
      <>
        <span className={cx({ "d-none": editId !== data._id })}>
          <Button
            className="text-nowrap mr-2"
            id={`save-${index}`}
            color="success"
            size="sm"
            onClick={this.handleIcon}
          >
            <i className="fas fa-check" />
          </Button>
          <Tooltip placement="left" target={`save-${index}`}>
            Save
          </Tooltip>
          <Button
            className="text-nowrap"
            id={`cancel-${index}`}
            color="danger"
            size="sm"
            onClick={this.cancleEdit}
          >
            <i className="fas fa-times" />
          </Button>

          <Tooltip placement="left" target={`cancel-${index}`}>
            Cancel
          </Tooltip>
        </span>
        <span className={cx({ "d-none": editId === data._id })}>
          <Button
            className="text-nowrap mr-2 editBtn"
            id={`edit-${index}`}
            color="success"
            size="sm"
            onClick={(e) => this.handleEdit(e, data)}
          >
            <img src={EditIcon} alt="" />
          </Button>
          <Tooltip placement="left" target={`edit-${index}`}>
            Edit
          </Tooltip>
          <Button
            className="text-nowrap editBtn"
            id={`remove-${index}`}
            color="danger"
            size="sm"
            onClick={(e) => this.remove(e, data)}
          >
            <img src={DeleteIcon} alt="" />
          </Button>
          <Tooltip placement="right" target={`remove-${index}`}>
            Delete
          </Tooltip>
        </span>
      </>
    );
  };
  handlePageChange = (pageNumber) => {
    const { limit } = this.state;
    let obj = {
      offset: (pageNumber - 1) * limit,
      loading: true,
      activePage: pageNumber,
    };
    this.setState(obj);
    this.fetch(obj);
  };
  render() {
    const {
      addNewHoliday,
      totalLength,
      holidays = [],
      editId,
      loading,
    } = this.state;
    // const type = localStorage.getItem("type");

    return (
      <>
        <Card>
          {/* {loading && <Loader />} */}
          <CardHeader>
            <div className="d-flex align-items-center">
              <h5 className="m-0">Public Holidays</h5>

              {isAdmin() ? (
                !addNewHoliday ? (
                  <button
                    className="ml-3 btn-icon-circle"
                    onClick={this.handleAddNewHoliday}
                  >
                    <i className="fas fa-plus" />
                  </button>
                ) : (
                  <button
                    className="ml-3 btn-icon-circle"
                    onClick={this.cancleAddNewHoliday}
                  >
                    <i className="fas fa-times" />
                  </button>
                )
              ) : null}
            </div>
          </CardHeader>

          <Collapse isOpen={addNewHoliday}>
            <CardBody className="position-relative" style={{ zIndex: 4 }}>
              <div className="date-pickers">
                <div className="date-pickersInput mb-md-0 mb-2">
                  {this.datepicker()}
                  {this.eventInput()}
                </div>
                <div className="date-pickersButtons">
                  <Button
                    color="success"
                    className="ml-sm-2"
                    disabled={loading}
                    onClick={this.handleIcon}
                  >
                    <i className="fa fa-dot-circle-o" /> Submit
                  </Button>
                  <Button
                    className="ml-sm-2"
                    color="danger"
                    disabled={loading}
                    onClick={this.cancleAddNewHoliday}
                  >
                    <i className="fas fa-times mr-2" />
                    Cancel
                  </Button>
                </div>
              </div>
            </CardBody>
          </Collapse>
          <div className={`f-w-loader ${loading ? "loader" : ""}`}>
            <div className="tableWrapper holidays">
              {holidays && holidays.length > 0 && (
                <Table striped responsive>
                  <thead>
                    <tr>
                      <th>S.No.</th>
                      <th>Date</th>
                      <th>Event</th>
                      {isAdmin() && <th></th>}
                    </tr>
                  </thead>
                  <tbody>
                    {holidays &&
                      holidays.length > 0 &&
                      holidays.map((data, index) => {
                        return (
                          <tr
                            key={index}
                            className={cx({
                              selected: editId === data._id,
                            })}
                          >
                            <td>{index + 1}</td>
                            <td>
                              {editId === data._id
                                ? this.datepicker()
                                : // : moment(data.date).format("DD/MM/YYYY")}
                                  moment(new Date(data.date)).format(
                                    "DD/MM/YYYY"
                                  )}
                              {/* {this.datepicker()} */}
                            </td>
                            <td>
                              {editId === data._id
                                ? this.eventInput()
                                : data.title}
                            </td>

                            {isAdmin() && (
                              <td className="text-right text-nowrap">
                                {this.actionButtons(editId, data, index)}
                              </td>
                            )}
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
              )}
              {/* {removeModal && ( */}
              <DeleteModal
                isOpen={this.state.removeModal}
                deleteRecord={this.deleteHoliday}
                loading={this.state.loading}
                id={this.state.id}
                toggle={this.remove}
                eventName={this.state.eventName}
              />
              {/* )} */}
              {loading ? (
                <Loader />
              ) : (
                holidays.length === 0 && (
                  <div className="no-records">
                    <h5 className="mb-0">
                      <i>No Records Found</i>
                    </h5>
                  </div>
                )
              )}
            </div>
          </div>
        </Card>
        {totalLength > 10 && (
          <CommonPagination
            activePage={this.state.activePage}
            itemsCountPerPage={10}
            totalItemsCount={totalLength}
            handlePageChange={this.handlePageChange}
          />
        )}
      </>
    );
  }
}
