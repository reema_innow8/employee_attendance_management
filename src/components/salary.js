import React from "react";
import DefaultLayout from "../containers/layout";
import { Table, Card, CardHeader, Button } from "reactstrap";
import { apiCall } from "../common/axios";
import { Link } from "react-router-dom";
import Loader from "../loader/index";
import EditIcon from "../assets/image/edit.svg";
import Tooltip from "./tooltip";
import AddSalary from "../assets/image/icons/addSalary.svg";
import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";
import CompileSalary from "../assets/image/icons/compile-salary.svg";
import CommonPagination from "../common/pagination";
import { isAdmin } from "../common/typeUser";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
class Salary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      salaryArr: [],
      activePage: 1,
      offset: 0,
      limit: 10,
      totalLength: 0,
      isOpen: false,
      salList: [],
      search: "",
      searchData: "",
    };
  }
  // filterRecords = () => {
  //   const { salaryArr } = this.state;
  //   const search = this.state.search.trim().replace(/ +/g, " ");
  //   if (!search) return salaryArr;
  //   return (
  //     salaryArr &&
  //     salaryArr.filter((data) => {
  //       let isTrue =
  //         data.employee.name.toLowerCase().includes(search) ||
  //         data.employee.empId.toLowerCase().includes(search);
  //       return isTrue;
  //     })
  //   );
  // };
  handleClick = () => {
    this.setState({
      loading: true,
      isOpen: !this.state.isOpen,
    });
    const user = JSON.parse(localStorage.getItem("user"));
    apiCall("salary/compile", { id: user.id }).then(async (res) => {
      const { status, data } = res.data;
      if (status === 200) {
        this.setState({
          loading: false,
          salList: data || [],
        });
      }
    });
  };

  exportToCSV = (csvData) => {
    const ws = XLSX.utils.json_to_sheet(csvData.data);
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    const data = new Blob([excelBuffer], {
      type:
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
    });
    FileSaver.saveAs(data, csvData.name + ".xlsx");
    this.setState({
      loading: false,
    });
  };

  compileSalary = () => {
    this.setState({
      loading: true,
      isOpen: !this.state.isOpen,
    });
    setTimeout(() => {
      apiCall("salary/download").then((result) => {
        const records = result.data.data || { name: "salaries", data: [] };
        this.exportToCSV(records);
      });
    }, 1000);
  };

  componentDidMount() {
    this.fetch();
  }

  handleEdit = (e, data) => {
    e.preventDefault();
    e.stopPropagation();

    this.props.history.push({
      pathname: "/salary/edit",
      state: data,
    });
  };

  fetch = (offsetObj) => {
    const { offset, limit, searchData } = this.state;
    let stateObj = {
      loading: true,
    };
    const userId = JSON.parse(localStorage.getItem("user")).id;
    let obj = {
      id: userId,
      limit,
    };
    if (offsetObj) {
      if (offsetObj.searchData) {
        obj.search = offsetObj.searchData;
        obj.offset = 0;
        stateObj.offset = 0;
        stateObj.activePage = 1;
      } else if (offsetObj.searchData !== '') {
        obj.search = searchData;
      }
    }
    this.setState(stateObj);
    if (offsetObj && offsetObj.offset >= 0) {
      obj.offset = offsetObj.offset;
    } else if (!(stateObj.offset === 0)) {
      obj.offset = offset;
    }

    apiCall("salary/fetch", obj)
      .then((res) => {
        const response = res.data;
        if (response.status === 200) {
          this.setState({
            loading: false,
            salaryArr:
              (response && response.data && response.data.salaries) || [],
            totalLength:
              (response && response.data && response.data.count) || [],
            searchData: obj.search || "",
          });
        } else {
          this.setState({
            loading: false,
            salaryArr: [],
            totalLength: 0,
          });
        }
      })
      .catch((error) => {
        if (error) {
          this.setState({
            loading: false,
          });
        }
      });
  };
  handlePageChange = (pageNumber) => {
    const { limit } = this.state;
    let obj = {
      offset: (pageNumber - 1) * limit,
      loading: true,
      activePage: pageNumber,
    };
    this.setState(obj);
    this.fetch(obj);
  };
  render() {
    const { loading, totalLength, salList, salaryArr } = this.state;

    // const searchRecords = this.filterRecords() || [];
    // const type = localStorage.getItem("type");
    return (
      <div>
        <DefaultLayout>
          <Card>
            {/* {loading && <Loader />} */}

            <CardHeader>
              <div className="salaryHeader">
                <h5>Salary of Employees</h5>
                {isAdmin() && (
                  <div className="text-center searchWrapper">
                    <input
                      type="search"
                      placeholder="Search by an employee name or an id"
                      required
                      onChange={(e) =>
                        this.fetch({ searchData: e.target.value })
                      }
                      className="search mb-0"
                    />
                  </div>
                )}
                <div className="salaryButtons">
                  <Link
                    to={{
                      pathname: "/salary/add",
                    }}
                    className="add-salary"
                  >
                    {isAdmin() && (
                      <Button
                        className="ml-3  mb-sm-0 mb-2"
                        color="primary"
                        disabled={loading}
                      >
                        <img src={AddSalary} className="mr-2" alt="" />
                        Add Salary
                      </Button>
                    )}
                  </Link>
                  {/* {isAdmin() && (
                    <Button
                      className="ml-3"
                      color="success"
                      disabled={loading}
                      onClick={this.compileSalary}
                    >
                      <img src={CompileSalary} className="mr-2" alt="" />
                      Compile Salary
                    </Button>
                  )} */}
                  {isAdmin() && (
                    <Button
                      className="ml-3"
                      color="success"
                      disabled={loading}
                      onClick={this.handleClick}
                    >
                      <img src={CompileSalary} className="mr-2" alt="" />
                      Compiled Salary
                    </Button>
                  )}
                </div>
              </div>
            </CardHeader>
            <div className={`f-w-loader ${loading ? "loader" : ""}`}>
              <div className="tableWrapper salary ">
                {/* {isAdmin() && (
                  <div className="text-center mt-2">
                    <input
                      type="search"
                      placeholder="Search by an employee name or an id"
                      required
                      onChange={(e) =>
                        this.setState({ search: e.target.value })
                      }
                      className="search"
                    />
                  </div>
                )} */}
                {salaryArr && salaryArr.length > 0 && (
                  <Table responsive striped>
                    <thead>
                      <tr>
                        <th className="text-nowrap">Employee ID</th>

                        <th>Name</th>
                        <th>Salary</th>

                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {salaryArr &&
                        salaryArr.length > 0 &&
                        salaryArr.map((data, index) => {
                          return (
                            <tr key={index}>
                              <td>
                                {data && data.employee && data.employee.empId}
                              </td>
                              <td>
                                {data && data.employee && data.employee.name}
                              </td>
                              <td>{data && data.salary}</td>

                              <td className="text-right">
                                {isAdmin() && (
                                  <>
                                    <Link
                                      to={{
                                        pathname: "/employees/edit",
                                      }}
                                    >
                                      <Button
                                        id={`edit-${index}`}
                                        className="text-nowrap editBtn"
                                        disabled={loading}
                                        color="success"
                                        size="sm"
                                        onClick={(e) =>
                                          this.handleEdit(e, data)
                                        }
                                      >
                                        <img src={EditIcon} alt="" />
                                      </Button>
                                    </Link>

                                    <Tooltip
                                      placement="left"
                                      target={`edit-${index}`}
                                    >
                                      Edit
                                    </Tooltip>
                                  </>
                                )}
                              </td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </Table>
                )}
                {loading ? (
                  <Loader />
                ) : (
                  salaryArr &&
                  salaryArr.length === 0 && (
                    <div className="no-records">
                      <h5 className="mb-0">
                        <i>No Records Found</i>
                      </h5>
                    </div>
                  )
                )}
              </div>
            </div>
            <Modal
              isOpen={this.state.isOpen}
              toggle={this.handleClick}
              className="leaveModal text-center"
              style={{ maxWidth: "800px", marginTop: "130px" }}
            >
              <ModalHeader>
                Compiled Salary
                <Button
                  className="ml-3 compileBtn"
                  color="success"
                  disabled={loading}
                  id="salary"
                  onClick={this.compileSalary}
                >
                  <i className="fas fa-download"></i>
                </Button>
                <Tooltip placement="left" target="salary">
                  Download
                </Tooltip>
              </ModalHeader>
              <ModalBody className={`f-w-loader ${loading ? "loader" : ""}`}>
                {loading && <Loader />}
                <div className="tableWrapper compiledSalary">
                  <Table striped responsive>
                    <thead>
                      <tr>
                        <th className="text-nowrap">S.No.</th>
                        <th className="text-nowrap">Emp ID</th>
                        <th className="text-nowrap">Name</th>
                        <th className="text-nowrap">Salary</th>
                        <th className="text-nowrap">LOP</th>
                        <th className="text-nowrap">Leave Balance</th>
                        <th className="text-nowrap">Final Salary</th>
                      </tr>
                    </thead>
                    <tbody>
                      {salList &&
                        salList.length > 0 &&
                        salList.map((sal, index) => {
                          return (
                            <tr key={index}>
                              <td>{sal.id}</td>
                              <td>{sal.emp_id}</td>
                              <td>{sal.employee}</td>
                              <td>{sal.salary}</td>
                              <td>{sal.lop}</td>
                              <td>{sal.leave_balance}</td>
                              <td>{sal.final_salary}</td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </Table>
                </div>
              </ModalBody>
            </Modal>
          </Card>
          {totalLength > 10 && (
            <CommonPagination
              activePage={this.state.activePage}
              itemsCountPerPage={10}
              totalItemsCount={totalLength}
              handlePageChange={this.handlePageChange}
            />
          )}
        </DefaultLayout>
      </div>
    );
  }
}
export default Salary;
