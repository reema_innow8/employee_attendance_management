import React from "react";

const Dashboard = React.lazy(() => import("./views/Dashboard"));

const routes = [
  { path: "/login", exact: true, name: "Login" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard }
];

export default routes;
