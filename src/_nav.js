import React from "react";
import { isAdmin } from "./common/typeUser";
import {
  Dashboard,
  Employee,
  Holiday,
  Setting,
  Suitcase,
  Salary,
} from "./assets/image/iconsComponent";
const settingMenu = [
  {
    name: (
      <>
        <Setting />
        Settings
      </>
    ),
    url: "/Setting",
  },
];

const adminMenu = [
  {
    name: (
      <>
        <Salary />
        Salary
      </>
    ),
    url: "/salary",
  },
  ...settingMenu,
];

export default {
  items: [
    {
      name: (
        <>
          <Dashboard />
          Dashboard
        </>
      ),
      url: "/dashboard",
    },
    {
      name: (
        <>
          <Employee />
          Employees
        </>
      ),
      url: "/employees",
    },
    {
      name: (
        <>
          <Holiday />
          Holidays
        </>
      ),
      url: "/holidays",
    },
    {
      name: (
        <>
          <Suitcase />
          Leaves
        </>
      ),
      url: "/leaves",
    },
    ...(isAdmin() ? adminMenu : settingMenu),
  ],
};
