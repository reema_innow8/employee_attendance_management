export const isLogin = () => {
  return localStorage && localStorage.getItem("isLoggedIn");
};
