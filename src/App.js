import React, { Component } from "react";
import { Switch, BrowserRouter } from "react-router-dom";
import { NotificationContainer } from "react-notifications";
import "./App.scss";
import ForgotPassword from "./components/forgotPassword";
import Salary from "./components/salary";
import Leaves from "./components/leaves";
import Holidays from "./components/holidays";
import Setting from "./components/setting";
import AddEmployee from "./views/Pages/AddEmployee/AddEmployee";
import AddSalary from "./views/Pages/AddSalary/AddSalary";
import EmployeeDetail from "./views/Pages/EmployeeDetail/EmployeeDetail";
import Employees from "./components/employees";
import Dashboard from "./views/Pages/Dashboard/Dashboard";
import Login from "./views/Pages/Login/Login";
import PrivateRoutes from "./components/PrivateRoute";
import PublicRoute from "./components/PublicRoute";

class App extends Component {
  render() {
    return (
      <>
        <BrowserRouter>
          <Switch>
            <PublicRoute path="/" exact component={Login} />

            <PrivateRoutes exact path="/dashboard" component={Dashboard} />
            <PrivateRoutes path="/employees" exact component={Employees} />
            <PrivateRoutes
              path="/employees/add"
              exact
              component={AddEmployee}
            />
            <PrivateRoutes path="/salary/add" exact component={AddSalary} />
            <PrivateRoutes
              path="/employees/edit"
              exact
              component={AddEmployee}
            />
            <PrivateRoutes path="/salary/edit" exact component={AddSalary} />
            <PrivateRoutes
              path="/employees/detail"
              exact
              component={EmployeeDetail}
            />
            <PrivateRoutes path="/setting" exact component={Setting} />
            <PrivateRoutes path="/holidays" exact component={Holidays} />
            <PrivateRoutes path="/leaves" exact component={Leaves} />
            <PrivateRoutes path="/salary" exact component={Salary} />
            <PublicRoute
              path="/forgot-password"
              exact
              component={ForgotPassword}
            />
          </Switch>
        </BrowserRouter>
        <NotificationContainer />
      </>
    );
  }
}

export default App;
