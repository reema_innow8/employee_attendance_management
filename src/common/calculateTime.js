import moment from "moment";

export const dateDifference = (startDate, endDate) => {
  let result = {
    days: 0,
    hours: 0,
    minutes: 0,
  };
  if (!startDate || !endDate) return result;
  const ticks = (new Date(endDate) - new Date(startDate)) / 1000;
  let delta = Math.abs(ticks);
  // calculate (and subtract) whole days
  result.days = Math.floor(delta / 86400);
  delta -= result.days * 86400;
  // calculate (and subtract) whole hours
  result.hours = Math.floor(delta / 3600) % 24;
  delta -= result.hours * 3600;
  // calculate (and subtract) whole minutes
  result.minutes = Math.floor(delta / 60) % 60;
  delta -= result.minutes * 60;
  return result;
};

export const minHrDayConverter = (time) => {
  let { days, hours, minutes } = time;
  if (minutes > 59) {
    hours += Math.floor(minutes / 60);
    minutes = minutes % 60;
  }
  if (hours > 24) {
    days += Math.floor(hours / 24);
    hours = hours % 24;
  }
  return { days, hours, minutes };
};

export const checkHoliday = (day, date) => {
  let isHoliday = false;
  if (day && date) {
    const weekoffs = JSON.parse(localStorage.getItem("weekoffs"));
    const holidays = JSON.parse(localStorage.getItem("holidays"));
    isHoliday = weekoffs.includes(day) || holidays.includes(date);
  }
  return isHoliday;
};

export const getDates = (startDate, endDate) => {
  const dates = [];
  let initial = new Date(startDate);
  const stop = new Date(endDate);
  while (initial <= stop) {
    dates.push(new Date(initial));
    initial = moment(initial).add(1, "days");
  }
  return dates;
};
