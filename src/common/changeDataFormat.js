import moment from "moment";

export const setWeekOffs = (data) => {
  const weekend = [];
  if (data) {
    for (let i of Object.keys(data)) {
      if (data[i] === true) {
        let day = i.charAt(0).toUpperCase() + i.slice(1, 3);
        weekend.push(day);
      }
    }
  }
  localStorage.setItem("weekoffs", JSON.stringify(weekend));
};

export const setPublicHolidays = (data) => {
  const offHolidays = [];
  if (data) {
    for (let i of data) {
      let date = moment(i.date).format("DD MMM");
      offHolidays.push(date);
    }
  }
  localStorage.setItem("holidays", JSON.stringify(offHolidays));
};
