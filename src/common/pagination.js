import React from "react";
import Pagination from "react-js-pagination";

class CommonPagination extends React.Component {
  render() {
    return (
      <div>
        <Pagination
          itemClass="page-item"
          linkClass="page-link"
          activeClass="active"
          className="mt-3 mx-auto w-fit-content"
          activePage={this.props.activePage}
          itemsCountPerPage={this.props.itemsCountPerPage}
          totalItemsCount={this.props.totalItemsCount}
          onChange={this.props.handlePageChange}
        />
      </div>
    );
  }
}

export default CommonPagination;
