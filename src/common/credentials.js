// export const backendUrl = "http://localhost:8000";
export const backendUrl = "http://3.7.30.2";

// allowed leaves per month
export const leavesPerMonth = 1.25;

// leaves for first joining month
export const firstMonthLeaves = (date) => {
  if (date) {
    if (1 <= date && date <= 15) {
      return 1.25;
    } else if (16 <= date && date <= 25) {
      return 0.5;
    } else if (26 <= date && date <= 27) {
      return 0.25;
    } else {
      return 0;
    }
  } else return 0;
}

// holiday working time value
export const holidayWorkingTimeValue = hrs => {
  if (hrs) {
    if (hrs < 5) {
      return 0;
    } else if (hrs < 7) {
      return 0.5;
    } else if (hrs < 9) {
      return 0.75;
    } else if (hrs >= 9) {
      return 1;
    } else {
      return 0;
    }
  } else return 0;
}

// Absent time value
export const absentTimeValue = hrs => {
  if (hrs) {
    if (6 <= hrs && hrs < 8) {
      return 0.25;
    } else if (4 <= hrs && hrs < 6) {
      return 0.5;
    } else if (hrs < 4) {
      return 1;
    } else {
      return 0;
    }
  } else return 0;
}

// Attendance text file format
export const attendancefileFormat = [
  "No	TMNo	EnNo	Name		GMNo	Mode	IN/OUT	Antipass	DateTime\n",
  "1	1	00000010	          	1	1	 	0	06.05.2020  10:00:00\n",
  "2	1	00000011	          	1	1	 	0	06.05.2020  10:10:24\n",
  "3	1	00000012	          	1	1	 	0	06.05.2020  10:14:29\n",
  "4	1	00000013	          	1	1	 	0	06.05.2020  11:14:33\n",
  "5	1	00000014	          	1	1	 	0	06.05.2020  10:30:40\n",
  "6	1	00000010	          	10	1	 	0	06.05.2020  19:00:00\n",
  "7	1	00000011	          	10	1	 	0	06.05.2020  18:10:24\n",
  "8	1	00000012	          	10	1	 	0	06.05.2020  19:14:29\n",
  "9	1	00000013	          	10	1	 	0	06.05.2020  15:14:33\n",
  "10	1	00000014	          	10	1	 	0	06.05.2020  19:30:40\n",
];
