/* eslint-disable no-unused-expressions */
import axois from "axios";
import { backendUrl } from "../common/credentials";
import { notificationManager } from "../common/notificationManager";

const isPublicRoute = (url) => {
  const publicRoutes = [
    "auth/login",
    "auth/forgot-password",
    "auth/verify-token",
  ];
  return publicRoutes.includes(url);
};

export const apiCall = async (url, data) => {
  const authToken = localStorage.getItem("authToken");
  // const start = new Date();
  return await axois
    .post(`${backendUrl}/${url}`, data, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        operation: isPublicRoute(url) ? "public" : "private",
        Authorization: `Bearer ${authToken || ""}`,
      },
    })
    .then((res) => {
      // const timeTaken = new Date() - start;
      // console.log(`${url}`, (timeTaken / 1000) + "sec");
      if (res && res.data && res.data.status === 401) {
        // eslint-disable-next-line react/react-in-jsx-scope
        localStorage.clear();
        localStorage.setItem("isLoggedIn", false);
        notificationManager(res.data);
        // eslint-disable-next-line react/react-in-jsx-scope
        // <Redirect
        //   to={{
        //     pathname: "/",
        //   }}
        // />;
      }
      return res;
    });
  // .catch(err => {
  //   if (err) return "";
  // });
};
