/* eslint-disable no-unused-expressions */
import React from "react";
import { NotificationManager } from "react-notifications";
import { Redirect } from "react-router-dom";

export const notificationManager = (obj) => {
  const { status, message } = obj;
  if (status === 200 && message !== "Fetch Successfully.") {
    NotificationManager.success(message, "Click me!", 1000, () => {
      alert("callback");
    });
  } else {
    NotificationManager.warning(message);

    // eslint-disable-next-line no-unused-expressions
    // eslint-disable-next-line react/react-in-jsx-scope

    <Redirect
      to={{
        pathname: "/",
      }}
    />;

    setTimeout(() => {
      window.location.reload();
    }, 3000);
  }
};
