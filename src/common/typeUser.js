export const isAdmin = () => {
    let result = false;
    const type = localStorage.getItem("type");
    if (type === "admin") result = true;
    return result;
}
