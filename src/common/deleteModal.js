import React from "react";
import { Button, Modal, ModalBody } from "reactstrap";
import { isAdmin } from "../common/typeUser";
class DeleteModal extends React.Component {
  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        toggle={this.props.toggle}
        className="leaveModal mt-5"
      >
        <ModalBody className="text-center">
          <h5>
            {this.props.employeeId && (
              <b>
                Are you sure you want to delete this{" "}
                <i>{`"${this.props.employeeId}"`}</i> record ?
              </b>
            )}
            {this.props.eventName && (
              <b>
                Are you sure you want to delete this{" "}
                <i> {`"${this.props.eventName}"`}</i> event ?
              </b>
            )}
            {this.props.employeeName && this.props.statusLeave === "sent" && (
              <b>
                Are you sure <i>{`"${this.props.employeeName}"`}</i> that you
                want to cancel this leave application ?
              </b>
            )}
            {this.props.employeeName &&
              this.props.statusLeave === "approved" &&
              !isAdmin() && (
                <b>
                  Are you sure <i>{`"${this.props.employeeName}"`}</i> that you
                  want to request cancellation ?
                </b>
              )}
            {this.props.employeeName &&
              this.props.statusLeave === "approved" &&
              isAdmin() && (
                <b>
                  Are you sure that you want to cancel{" "}
                  <i>{`"${this.props.employeeName}'s"`}</i> leave application ?
                </b>
              )}
          </h5>
          <div className="text-center deleteBtn pt-2 mt-4">
            <Button
              color="secondary"
              onClick={this.props.toggle}
              className="cancelBtn"
              disabled={this.props.loading}
            >
              Cancel
            </Button>{" "}
            <Button
              color="primary"
              onClick={() => this.props.deleteRecord(this.props.id)}
              disabled={this.props.loading}
            >
              Yes, Confirmed
            </Button>
          </div>
        </ModalBody>
      </Modal>
    );
  }
}

export default DeleteModal;
