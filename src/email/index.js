import React from "react";
const blue = "#3b64ef";
const black = "#212529";
const white = "#fff";
const mainTableStyle = {
  width: 500,
  marginLeft: "auto",
  marginRight: "auto",
  color: black,
  border: `1px solid #dddddd`,
};
const mt0 = {
  marginTop: 0,
};
const mt1 = {
  marginTop: 1,
};
const mt2 = {
  marginTop: 2,
};
const mt3 = {
  marginTop: 3,
};
const mt4 = {
  marginTop: 4,
};
const mt5 = {
  marginTop: 5,
};
const mt6 = {
  marginTop: 6,
};
const mb0 = {
  marginBottom: 0,
};
const mb1 = {
  marginBottom: 1,
};
const mb2 = {
  marginBottom: 2,
};
const mb3 = {
  marginBottom: 3,
};
const mb4 = {
  marginBottom: 4,
};
const mb5 = {
  marginBottom: 5,
};
const mb6 = {
  marginBottom: 6,
};
const my0 = {
  ...mt0,
  ...mb0,
};
const titleStyle = { fontSize: 26, ...mt0 };
const btn = {
  background: blue,
  color: white,
  padding: "8px 16px",
  borderRadius: 6,
  textDecoration: "none",
  display: "table",
  ...mt6,
  ...mb6,
};

const center = { textAlign: "center" };
const marginCenter = { marginLeft: "auto", marginRight: "auto" };

const loremText =
  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";

const ForgotPassword = (
  <table style={{ width: "100%" }}>
    <tbody>
      <tr>
        <td>
          <h2 style={{ ...center, ...titleStyle }}>Forgot Password</h2>
          <h3 style={my0}>Hi Razat,</h3>
          <p>{loremText}</p>
          <a style={{ ...btn, ...marginCenter }} href="#">
            Click Here
          </a>
          <p>{loremText}</p>
        </td>
      </tr>
    </tbody>
  </table>
);

const NewEmployeeAdd = (
  <table style={{ width: "100%" }}>
    <tbody>
      <tr>
        <td>
          <h2 style={{ ...center, ...titleStyle }}>Added New Employee</h2>
          <h3 style={my0}>Hi Razat,</h3>
          <p>Your username and password below:</p>
          <p>
            <b>Username:</b> razat
          </p>
          <p>
            <b>Password:</b> 123456
          </p>
          <p>{loremText}</p>
        </td>
      </tr>
    </tbody>
  </table>
);
const LeaveAccepted = (
  <table style={{ width: "100%" }}>
    <tbody>
      <tr>
        <td>
          <h2 style={{ ...center, ...titleStyle }}>Leave Status</h2>
          <h3 style={my0}>Hi Razat,</h3>
          <p>Your Leave Approved</p>
        </td>
      </tr>
    </tbody>
  </table>
);

export default () => (
  <div>
    <table style={mainTableStyle}>
      <tbody>
        <tr>
          <td style={{ padding: 30 }}>
            {ForgotPassword}
            {NewEmployeeAdd}
            {LeaveAccepted}
          </td>
        </tr>
      </tbody>
    </table>
  </div>
);
